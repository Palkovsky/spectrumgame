package pl.dawidmacek.spectrum;

import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import java.util.ArrayList;
import java.util.List;

import pl.dawidmacek.spectrum.models.AudioFile;


public class AndroidLauncher extends AndroidApplication implements SpectrumGame.SystemCommunicator {

    private static final String[] SUPPORTED_FORMATS = {"mp3", "m4a", "flac"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useImmersiveMode = true;
        config.numSamples = 4;
        initialize(new SpectrumGame(this), config);
    }

    public List<AudioFile> getAudioFiles() {
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String[] projection = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION
        };
        final String sortOrder = MediaStore.Audio.AudioColumns.TITLE + " COLLATE LOCALIZED ASC";
        List<AudioFile> audioFiles = new ArrayList<>();

        Cursor cursor = null;
        try {
            Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            cursor = getContentResolver().query(uri, projection, selection, null, sortOrder);
            if (cursor != null) {
                cursor.moveToFirst();

                while (!cursor.isAfterLast()) {
                    String title = cursor.getString(0);
                    String artist = cursor.getString(1);
                    String path = cursor.getString(2);
                    String displayName = cursor.getString(3);
                    String songDuration = cursor.getString(4);

                    cursor.moveToNext();
                    if (path != null) {

                        for (String supportedExt : SUPPORTED_FORMATS) {
                            if (path.endsWith("." + supportedExt)) {
                                audioFiles.add(new AudioFile(title, artist, path, displayName, Long.parseLong(songDuration)));
                                break;
                            }
                        }
                    }
                }

            }

        } catch (Exception e) {
            Log.e("ERROR", e.toString());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return audioFiles;
    }

    @Override
    public void sendMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean hasMicrophone() {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_MICROPHONE);
    }
}
