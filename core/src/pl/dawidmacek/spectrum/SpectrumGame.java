package pl.dawidmacek.spectrum;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


import java.util.LinkedList;
import java.util.List;

import aurelienribon.tweenengine.Tween;
import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.drawing.Fonts;
import pl.dawidmacek.spectrum.drawing.NumberDrawer;
import pl.dawidmacek.spectrum.models.AudioFile;
import pl.dawidmacek.spectrum.objects.ProgressBar;
import pl.dawidmacek.spectrum.objects.notes.FillUpNote;
import pl.dawidmacek.spectrum.objects.notes.SlideNote;
import pl.dawidmacek.spectrum.screens.BaseScreen;
import pl.dawidmacek.spectrum.screens.ChooserScreen;
import pl.dawidmacek.spectrum.screens.GameModeScreen;
import pl.dawidmacek.spectrum.ui.transitions.FadeTransitionScreen;
import pl.dawidmacek.spectrum.ui.transitions.TransitionScreen;
import pl.dawidmacek.spectrum.ui.tweens.ActorTween;
import pl.dawidmacek.spectrum.ui.tweens.ColorTween;
import pl.dawidmacek.spectrum.ui.tweens.FillUpNoteTween;
import pl.dawidmacek.spectrum.ui.tweens.ProgressBarTween;
import pl.dawidmacek.spectrum.ui.tweens.SlideNoteTween;
import pl.dawidmacek.spectrum.ui.tweens.SpriteTween;
import pl.dawidmacek.spectrum.ui.tweens.Vector2Tween;
import pl.dawidmacek.spectrum.utils.Content;

public class SpectrumGame extends Game {

    public static Content res;
    public static Preferences prefs;
    public static NumberDrawer numberDrawer;

    public Skin skin;
    public PolygonSpriteBatch spriteBatch;
    public SystemCommunicator communicator;
    public LinkedList<BaseScreen> screens;

    public SpectrumGame(SystemCommunicator communicator) {
        this.communicator = communicator;
    }

    @Override
    public void create() {
        res = new Content();
        prefs = Gdx.app.getPreferences("SPECTRUM_GAME PREFERENCES");
        loadAssets();
        skin = new Skin(Gdx.files.internal("skins/uiskin.json"));
        numberDrawer = new NumberDrawer();
        spriteBatch = new PolygonSpriteBatch();
        screens = new LinkedList<BaseScreen>();

        Tween.setCombinedAttributesLimit(4);
        Tween.registerAccessor(Sprite.class, new SpriteTween());
        Tween.registerAccessor(Actor.class, new ActorTween());
        Tween.registerAccessor(Vector2.class, new Vector2Tween());
        Tween.registerAccessor(Color.class, new ColorTween());
        Tween.registerAccessor(SlideNote.class, new SlideNoteTween());
        Tween.registerAccessor(FillUpNote.class, new FillUpNoteTween());
        Tween.registerAccessor(ProgressBar.class, new ProgressBarTween());
        Tween.setWaypointsLimit(10);
        World.setVelocityThreshold(1);

        Fonts.getInstance(); //to create instance with all fonts

        Gdx.input.setCatchBackKey(true);
        setScreen(new ChooserScreen(this));
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
        res.dispose();
        Fonts.getInstance().dispose();
    }

    public void setScreen(BaseScreen screen) {
        super.setScreen(screen);
        screens.add(screen);
    }

    public void popScreen(){
        if(screens.size() >= 2){
            screens.pollLast();
            setScreen(screens.peekLast());
        }
    }

    public BaseScreen getBaseScreen() {
        if (screen instanceof BaseScreen)
            return ((BaseScreen) screen);
        return null;
    }


    public void fadeTransition(final BaseScreen screen) {
        fadeTransition(screen, true);
    }

    public void fadeTransition(final BaseScreen screen, boolean animateFirst) {
        fadeTransition(screen, animateFirst, 1.5f);
    }

    public void fadeTransition(final BaseScreen screen, boolean animateFirst, float duration) {
        BaseScreen currentScreen = getBaseScreen();

        if (currentScreen != null) {
            FadeTransitionScreen transition = new FadeTransitionScreen(this, getBaseScreen(), screen, Dimen.WIDTH, Dimen.HEIGHT, duration);
            transition.animateFirst(animateFirst);
            setScreen(transition);
        }
    }


    public void transition(TransitionScreen transitionScreen) {
        setScreen(transitionScreen);
    }

    private void loadAssets() {
        res.loadTexture("graphics/white.png", "WHITE");
    }

    public interface SystemCommunicator {
        List<AudioFile> getAudioFiles();

        void sendMessage(String message);

        boolean hasMicrophone();
    }
}
