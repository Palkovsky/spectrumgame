package pl.dawidmacek.spectrum.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class RandomUtil extends Random {

    public int randInt(int max) {
        return nextInt((max) + 1);
    }

    public int randInt(int min, int max) {
        return nextInt((max - min) + 1) + min;
    }

    public float randFloat(float min, float max) {
        return nextFloat() * (max - min) + min;
    }

    public Integer randInt(int min, int max, List<Integer> exceptions) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = min; i < max; i++) {
            if (!exceptions.contains(i))
                list.add(i);
        }
        return nextInt((max - min) + 1) + min;
    }

    public int randInt(float min, float max) {
        return nextInt(((int) max - (int) min) + 1) + (int) min;
    }

    public int randOne() {
        if (nextBoolean())
            return 1;
        else
            return -1;
    }

    public <T extends Enum<?>> T randomEnum(Class<T> c) {
        int x = nextInt(c.getEnumConstants().length);
        return c.getEnumConstants()[x];
    }

    public short[] sfxNoise(int n) {
        short[] samples = new short[n];
        for (int i = 0; i < n; i++) {
            samples[i] = (short) (randInt(0, 30000) * (NumberUtils.signum(i % 2 == 0)));
        }
        return samples;
    }
}
