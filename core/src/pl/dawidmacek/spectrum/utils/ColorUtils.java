package pl.dawidmacek.spectrum.utils;

import com.badlogic.gdx.graphics.Color;

import java.util.Random;

public class ColorUtils {

    public static Color randomColor() {
        Random r = new Random();
        return new Color(r.nextInt(255) / 255f, r.nextInt(255) / 255f, r.nextInt(255) / 255f, 1);
    }

    public static Color parseRGB(String rgb) {
        return parseRGB(rgb, 1);
    }

    public static Color parseRGB(String rgb, float a) {
        if (rgb.startsWith("#"))
            rgb = rgb.substring(1, 6);
        float r = Integer.parseInt(rgb.substring(0, 2), 16) / 255f;
        float g = Integer.parseInt(rgb.substring(2, 4), 16) / 255f;
        float b = Integer.parseInt(rgb.substring(4, 6), 16) / 255f;
        return new Color(r, g, b, a);
    }

    public static boolean areColorsTheSame(Color colorA, Color colorB) {
        return (colorA != null && colorB != null && colorA.r == colorB.r && colorA.g == colorB.g && colorA.b == colorB.b);
    }

    public static boolean isCloserToBlack(Color color) {
        float blackDistance = color.r + color.g + color.b;
        float whiteDistance = (1 - color.r) + (1 - color.g) + (1 - color.b);
        return blackDistance > whiteDistance;
    }
}
