package pl.dawidmacek.spectrum.utils;

import java.util.HashMap;

public class Bundle {

    private HashMap<String, Object> bundle = new HashMap<String, Object>();

    public Bundle() {
    }

    //Putters

    public void putExtra(Integer integer, String key) {
        bundle.put(key, integer);
    }

    public void putExtra(String string, String key) {
        bundle.put(key, string);
    }

    public void putExtra(Boolean bool, String key) {
        bundle.put(key, bool);
    }

    public void putExtra(Float num, String key) {
        bundle.put(key, num);
    }

    public void putExtra(Object obj, String key) {
        bundle.put(key, obj);
    }

    //Getters

    public Integer getInteger(String key) {
        return (Integer) bundle.get(key);
    }

    public String getString(String key) {
        return (String) bundle.get(key);
    }

    public Boolean getBoolean(String key) {
        return (Boolean) bundle.get(key);
    }

    public Float getFloat(String key) {
        return (Float) bundle.get(key);
    }

    public Object getObject(String key) {
        return bundle.get(key);
    }

    //Utils
    public boolean isEmpty() {
        return bundle.isEmpty();
    }

    public boolean contains(String key) {
        return bundle.containsKey(key);
    }

    public void dispose() {
        bundle.clear();
    }
}