package pl.dawidmacek.spectrum.utils;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

public class Box2DHelper {

    public static void disposeBody(Body body) {
        if (body != null) {
            for (Fixture fixture : body.getFixtureList()) {
                fixture.setUserData(null);
                body.destroyFixture(fixture);
            }
            if (body.getWorld() != null)
                body.getWorld().destroyBody(body);
            body.setUserData(null);
        }
    }

}
