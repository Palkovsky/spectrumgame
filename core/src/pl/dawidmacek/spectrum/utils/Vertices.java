package pl.dawidmacek.spectrum.utils;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

public class Vertices {

    public static float[] ring(float x, float y, float radius, float thickness, double startAngle, double endAngle) {
        return ring(x, y, radius, thickness, startAngle, endAngle, 0);
    }

    public static float[] ring(float x, float y, float radius, float thickness, double startAngle, double endAngle, float accuracy) {
        if (endAngle < startAngle) {
            double temp = startAngle;
            startAngle = endAngle;
            endAngle = temp;
        }

        startAngle = Math.toRadians(startAngle);
        endAngle = Math.toRadians(endAngle);

        List<Float> vertices = new ArrayList<Float>();
        List<Vector2> values = new ArrayList<Vector2>();
        float longRadius = radius + thickness;
        double theta = startAngle;


        while (theta <= endAngle) {
            values.add(new Vector2((float) Math.sin(theta), (float) Math.cos(theta)));
            if (accuracy == 0) {
                if (Math.abs(theta - endAngle) < 0.1f)
                    theta += 0.01f;
                else
                    theta += 0.1f;
            } else {
                theta += accuracy;
            }
        }

        for (int i = 0; i < values.size(); i++) {
            Vector2 value = values.get(i);
            vertices.add(x + radius * value.x);
            vertices.add(y + radius * value.y);
        }

        for (int i = values.size() - 1; i >= 0; i--) {
            Vector2 value = values.get(i);
            vertices.add(x + longRadius * value.x);
            vertices.add(y + longRadius * value.y);
        }


        float[] v = new float[vertices.size()];
        for (int i = 0; i < vertices.size(); i++)
            v[i] = vertices.get(i);

        return v;
    }

    public static float[] circleSegment(float x, float y, float radius, double startAngle, double endAngle) {
        if (endAngle < startAngle) {
            double temp = startAngle;
            startAngle = endAngle;
            endAngle = temp;
        }

        startAngle = Math.toRadians(startAngle);
        endAngle = Math.toRadians(endAngle);

        List<Float> vertices = new ArrayList<Float>();
        vertices.add(x);
        vertices.add(y);


        double theta = startAngle;
        while (theta <= endAngle) {
            double pointX = x + radius * Math.sin(theta);
            double pointY = y + radius * Math.cos(theta);
            vertices.add((float) pointX);
            vertices.add((float) pointY);
            if (Math.abs(theta - endAngle) < 0.1f)
                theta += 0.01f;
            else
                theta += 0.1f;
        }

        float[] v = new float[vertices.size()];
        for (int i = 0; i < vertices.size(); i++)
            v[i] = vertices.get(i);

        return v;
    }

    public static float[] arc(float x, float y, float radius, double startAngle, double endAngle) {
        return arc(x, y, radius, startAngle, endAngle, 0);
    }

    public static float[] arc(float x, float y, float radius, double startAngle, double endAngle, float accuracy) {
        if (endAngle < startAngle) {
            double temp = startAngle;
            startAngle = endAngle;
            endAngle = temp;
        }

        startAngle = Math.toRadians(startAngle);
        endAngle = Math.toRadians(endAngle);

        List<Float> vertices = new ArrayList<Float>();


        double theta = startAngle;
        while (theta <= endAngle) {
            double pointX = x + radius * Math.sin(theta);
            double pointY = y + radius * Math.cos(theta);
            vertices.add((float) pointX);
            vertices.add((float) pointY);
            if (accuracy == 0) {
                if (Math.abs(theta - endAngle) < 0.1f)
                    theta += 0.01f;
                else
                    theta += 0.1f;
            } else {
                theta += accuracy;
            }
        }

        float[] v = new float[vertices.size()];
        for (int i = 0; i < vertices.size(); i++)
            v[i] = vertices.get(i);

        return v;
    }

    public static float[] triangle(float x, float y, float h, float a, double angle) {
        float[] vertices = new float[6];

        //Top point
        vertices[0] = x;
        vertices[1] = y + (2f * h) / 3f;

        //Left point
        vertices[2] = x - a;
        vertices[3] = y - h / 3f;

        //Right point
        vertices[4] = x + a;
        vertices[5] = y - h / 3f;

        return rotateVertices(x, y, vertices, angle);
    }

    public static float[] rectangle(float centerX, float centerY, float a, float b, double angle) {
        float[] vertices = new float[8];

        //Left Bottom
        vertices[0] = centerX - a / 2;
        vertices[1] = centerY - b / 2;

        //Left Top
        vertices[2] = centerX - a / 2;
        vertices[3] = centerY + b / 2;

        //Right Top
        vertices[4] = centerX + a / 2;
        vertices[5] = centerY + b / 2;

        //Right Bottom
        vertices[6] = centerX + a / 2;
        vertices[7] = centerY - b / 2;

        return rotateVertices(centerX, centerY, vertices, angle);
    }


    public static float[] regularPolygon(float x, float y, float r, int n, float rotation) {
        float[] vertices = new float[n * 2];
        int j = 0;
        for (int i = 0; i < n; i++) {
            vertices[j] = x + r * (float) Math.cos(2 * Math.PI * i / n);
            vertices[j + 1] = y + r * (float) Math.sin(2 * Math.PI * i / n);
            j += 2;
        }
        return rotateVertices(x, y, vertices, rotation);
    }

    public static float[] rotateVertices(float pivotX, float pivotY, float[] vertices, double angle) {
        Polygon pol = new Polygon(vertices);
        pol.setOrigin(pivotX, pivotY);
        pol.setRotation((float) angle);
        return pol.getTransformedVertices();
    }
}
