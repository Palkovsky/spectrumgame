package pl.dawidmacek.spectrum.utils;


import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

public class Points {

    public static float getAngle(Vector2 pointA, Vector2 pointB) {
        float angle = (float) Math.toDegrees(Math.atan2(pointB.y - pointA.y, pointB.x - pointA.x));
        angle = (angle < 0) ? angle + 360 : angle;
        return angle;
    }

    public static float getLength(Vector2 pointA, Vector2 pointB) {
        return (float) Math.sqrt(Math.pow(pointB.x - pointA.x, 2) + Math.pow(pointB.y - pointA.y, 2));
    }

    public static float getDistance(Vector2[] points) {
        float distance = 0;
        for (int i = 1; i < points.length; i++) {
            distance += Points.getLength(points[i - 1], points[i]);
        }
        return distance;
    }

    public static float[] cubicBezier(Vector2 pointA, Vector2 pointB, Vector2 pointC) {
        float t = 0;
        List<Vector2> bezierPoints = new ArrayList<Vector2>();

        while (t <= 1) {
            float x = (float) Math.pow(1 - t, 2) * pointA.x + 2 * (1 - t) * t * pointC.x + (float) Math.pow(t, 2) * pointB.x;
            float y = (float) Math.pow(1 - t, 2) * pointA.y + 2 * (1 - t) * t * pointC.y + (float) Math.pow(t, 2) * pointB.y;
            bezierPoints.add(new Vector2(x, y));

            t = (t >= .9f) ? t + .01f : t + .1f;
        }


        float[] vertices = new float[bezierPoints.size() * 2];
        for (int i = 0; i < vertices.length; i += 2) {
            vertices[i] = bezierPoints.get(i / 2).x;
            vertices[i + 1] = bezierPoints.get(i / 2).y;
        }
        return vertices;
    }

    public static Vector2[] verticesToVectors(float[] vertices) {
        Vector2[] vectors = new Vector2[vertices.length / 2];
        for (int i = 0; i < vertices.length; i += 2) {
            vectors[i / 2] = new Vector2(vertices[i], vertices[i + 1]);
        }
        return vectors;
    }

    public static float[] vectorsToVertices(Vector2[] vectors) {
        float[] vertices = new float[vectors.length * 2];
        int j = 0;
        for (Vector2 vector : vectors) {
            vertices[j] = vector.x;
            vertices[j + 1] = vector.y;
            j += 2;
        }
        return vertices;
    }
}
