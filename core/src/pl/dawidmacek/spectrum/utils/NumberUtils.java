package pl.dawidmacek.spectrum.utils;

import java.util.ArrayList;
import java.util.List;

public class NumberUtils {

    public static int highestCommonDivider(int a, int b) {
        while (a != b) {
            if (a > b)
                a -= b;
            else
                b -= a;
        }

        return a;
    }

    public static List<Integer> getDivisors(int num) {
        List<Integer> divisors = new ArrayList<Integer>();
        divisors.add(1);
        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                divisors.add(i);
            }
        }
        return divisors;
    }

    public static double scale(final double valueIn, final double baseMin, final double baseMax, final double limitMin, final double limitMax) {
        return ((limitMax - limitMin) * (valueIn - baseMin) / (baseMax - baseMin)) + limitMin;
    }

    public static int signum(boolean v) {
        if (v)
            return 1;
        else
            return -1;

    }

    public static float getAngle(float x1, float y1, float x2, float y2) {
        float angle = (float) Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }
}