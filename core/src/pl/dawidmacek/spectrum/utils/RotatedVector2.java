package pl.dawidmacek.spectrum.utils;

import com.badlogic.gdx.math.Vector2;

public class RotatedVector2 extends Vector2 {

    public RotatedVector2(double angle) {
        this.angle = angle;
    }

    public RotatedVector2(float x, float y, double angle) {
        super(x, y);
        this.angle = angle;
    }

    public RotatedVector2(Vector2 v, double angle) {
        super(v);
        this.angle = angle;
    }

    public double angle;
}
