package pl.dawidmacek.spectrum.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.files.FileHandle;

import net.sourceforge.jaad.Play;

import java.util.Arrays;

import edu.emory.mathcs.jtransforms.fft.FloatFFT_1D;
import pl.dawidmacek.spectrum.pcmgdx.wrapper.PCMPlayer;
import pl.dawidmacek.spectrum.pcmgdx.decoders.SampleFrame;

public class Player {

    private static Player instance = null;

    private PCMPlayer playbackPlayer;
    private PCMPlayer spectrumPlayer;

    public static final float SPECTRUM_PLAYER_OFFSET = 1.5f;

    private float[] spectrum = null;
    private FloatFFT_1D fft;

    private Player() {
        fft = new FloatFFT_1D(32);
        Gdx.app.addLifecycleListener(new LifecycleListener() {
            @Override
            public void pause() {
                Player.this.pause();
            }

            @Override
            public void resume() {
                Player.this.resume();
            }

            @Override
            public void dispose() {
                Player.this.dispose();
            }
        });
    }


    public static Player getInstance() {
        if (instance == null)
            instance = new Player();
        return instance;
    }


    public void stop() {
        if (playbackPlayer != null) {
            playbackPlayer.reset();
            playbackPlayer.pause();
        }
        if (spectrumPlayer != null) {
            spectrumPlayer.reset();
            spectrumPlayer.pause();
            spectrumPlayer.setProgress(SPECTRUM_PLAYER_OFFSET);
        }
    }

    public void dispose() {
        if (playbackPlayer != null) {
            playbackPlayer.dispose();
            playbackPlayer = null;
        }
        if (spectrumPlayer != null) {
            spectrumPlayer.dispose();
            spectrumPlayer = null;
        }
    }

    public void play(String path, boolean internal) {
        stop();
        dispose();
        FileHandle audioHandle = (internal) ? Gdx.files.internal(path) : Gdx.files.absolute(path);
        playbackPlayer = new PCMPlayer(audioHandle, false);
        spectrumPlayer = new PCMPlayer(audioHandle, false);
        spectrumPlayer.setVolume(0);
        spectrumPlayer.setProgress(SPECTRUM_PLAYER_OFFSET);
        spectrumPlayer.setFrameListener(new PCMPlayer.PCMPlayerFrameListener() {
            @Override
            public void onNewFrame(SampleFrame samples) {
                if (samples != null) {
                    float[] spec = new float[samples.getData().length];
                    for (int i = 0; i < samples.getData().length; i++) {
                        spec[i] = samples.getData()[i];
                    }

                    fft = new FloatFFT_1D(64);

                    spectrum = spec;
                    fft.complexForward(spec);
                }

                System.out.println("PROG: " + spectrumPlayer.getProgress());
            }
        });
        playbackPlayer.play();
        spectrumPlayer.play();
    }


    public void pause() {
        if (playbackPlayer != null)
            playbackPlayer.pause();
        if (spectrumPlayer != null)
            spectrumPlayer.pause();
    }

    public void resume() {
        if (playbackPlayer != null)
            playbackPlayer.unpause();
        if (spectrumPlayer != null)
            spectrumPlayer.unpause();
    }


    public float[] getSpectrum(int bars) {
        if (spectrum != null) {

            int chunkSize = spectrum.length / bars;
            int counter = 0;
            int len = spectrum.length;
            float chunks[][] = new float[bars][chunkSize];
            float avgs[] = new float[bars];

            for (int i = 0; i < len - chunkSize + 1; i += chunkSize) {
                counter++;
                if (counter < chunks.length && i + chunkSize < spectrum.length)
                    chunks[counter] = Arrays.copyOfRange(spectrum, i, i + chunkSize);
            }

            for (int i = 0; i < chunks.length; i++) {
                float[] chunk = chunks[i];
                float avg = 0;
                for (float item : chunk)
                    avg += item;
                avg = avg / chunk.length;
                avgs[i] = avg;//Math.abs(avg);
            }


            //sort asc, then change it for desc
            Arrays.sort(avgs);
            for (int i = 0; i < avgs.length / 2; ++i) {
                float temp = avgs[i];
                avgs[i] = avgs[avgs.length - i - 1];
                avgs[avgs.length - i - 1] = temp;
            }


            return avgs;
        }

        return new float[0];
    }


    public boolean isPlaying() {
        return (playbackPlayer != null && playbackPlayer.getState() == PCMPlayer.PlayerState.PLAYING);
    }

    public void setSpectrum(float[] spectrum) {
        this.spectrum = spectrum;
    }

    public float getProgress(){
        if(playbackPlayer != null)
            return playbackPlayer.getProgress();
        return 0;
    }

    public boolean isValidFile(String filePath, boolean internal) {
        FileHandle audioHandle = (internal) ? Gdx.files.internal(filePath) : Gdx.files.absolute(filePath);
        try {
            new PCMPlayer(audioHandle, true);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}