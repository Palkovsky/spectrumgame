package pl.dawidmacek.spectrum.utils;

import com.badlogic.gdx.math.Vector2;

public class Units {
    public static final int PPM = 100;

    public static float getPPM(float pixels) {
        return pixels / PPM;
    }

    public static float getPixels(float PPM) {
        return PPM * Units.PPM;
    }

    public static Vector2 getPPM(Vector2 v2) {
        return new Vector2(v2.x / 100, v2.y / 100);
    }

    public static Vector2 getPixels(Vector2 v2) {
        return new Vector2(v2.x * 100, v2.y * 100);
    }
}