package pl.dawidmacek.spectrum.utils;

import java.util.Random;

public class CustomArrayUtils {

    private static Random random;

    /**
     * Code from method java.util.Collections.shuffle();
     */
    public static void shuffle(float[] array) {
        if (random == null) random = new Random();
        int count = array.length;
        for (int i = count; i > 1; i--) {
            swap(array, i - 1, random.nextInt(i));
        }
    }

    public static float[] partitionate(float[] array, int n) {

        float[] outputArray = new float[array.length];

        int w = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i; j < array.length; j += n) {
                outputArray[w] = array[j];
                w++;
            }
        }

        return outputArray;
    }

    private static void swap(float[] array, int i, int j) {
        float temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static float avg(float[] array) {
        float avg = 0;
        for (float value : array)
            avg += value;
        avg /= array.length;
        return avg;
    }
}