package pl.dawidmacek.spectrum.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Converter {

    private static final Pixmap helperPixmap = new Pixmap(1, 1, Pixmap.Format.RGB565);

    public static Drawable toDrawable(Texture texture) {
        return new TextureRegionDrawable(new TextureRegion(texture));
    }

    public static Drawable toDrawable(Color color) {
        helperPixmap.setColor(color);
        helperPixmap.fill();
        return new Image(new Texture(helperPixmap)).getDrawable();
    }

    public static Vector2 convertCoordinates(OrthographicCamera camera, float x, float y) {
        Vector3 vector = new Vector3();

        vector.z = 0;
        vector.x = x;
        vector.y = y;

        vector = camera.unproject(vector);
        return new Vector2(vector.x, vector.y);
    }

    public static Vector2 getStageLocation(Actor actor) {
        return actor.localToStageCoordinates(new Vector2(0, 0));
    }

    public static String longToSeconds(long value) {
        //Sets up timer
        float total = value / 1000f;

        int totalMinutes = (int) total / 60;
        int totalSeconds = (int) total % 60;

        return format(totalMinutes) + ":" + format(totalSeconds);
    }

    private static String format(int value) {
        return (value < 10) ? "0" + value : value + "";
    }
}