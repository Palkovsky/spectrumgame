package pl.dawidmacek.spectrum.utils;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.LifecycleListener;
import com.badlogic.gdx.audio.AudioDevice;
import com.badlogic.gdx.audio.AudioRecorder;

import java.util.Arrays;

import edu.emory.mathcs.jtransforms.fft.FloatFFT_1D;

public class Recorder {

    private static Recorder instance = null;

    private Thread recordThread;
    private float[] spectrum = null;
    private boolean recording = false;
    private boolean paused = false;
    private FloatFFT_1D fft;
    private AudioDevice audioDevice = null;
    private AudioRecorder recorder = null;

    private Recorder() {
        fft = new FloatFFT_1D(32);
        Gdx.app.addLifecycleListener(new LifecycleListener() {
            @Override
            public void pause() {
                Recorder.this.pause();
            }

            @Override
            public void resume() {
                Recorder.this.resume();
            }

            @Override
            public void dispose() {
                Recorder.this.stop();
                Recorder.this.dispose();
            }
        });
    }

    public static Recorder getInstance() {
        if (instance == null)
            instance = new Recorder();
        return instance;
    }

    public void start() {
        this.recording = true;
        newThread();
        recordThread.start();
    }

    public void stop(){
        if (recordThread != null) {
            recordThread.interrupt();
            while (recordThread.isAlive()) {
            }
            this.spectrum = null;
            this.recording = false;
        }
    }

    private void newThread() {
        recordThread = new Thread(new Runnable() {
            @Override
            public void run() {

                while (recording && !paused && !Thread.currentThread().isInterrupted()) {
                    if (recorder == null) {
                        recorder = Gdx.audio.newAudioRecorder(44100, false);
                    }
                    if (audioDevice == null) {
                        audioDevice = Gdx.audio.newAudioDevice(44100, false);
                    }

                    short[] samples = new short[1024]; // 1024 samples

                    recorder.read(samples, 0, samples.length);

                    float[] spec = new float[samples.length];
                    for (int i = 0; i < samples.length; i++) {
                        spec[i] = samples[i] * 3;
                    }
                    spectrum = spec;
                    fft.complexForward(spec);

                    //audioDevice.writeSamples(samples, 0, samples.length);
                }

            }
        });
    }

    public void dispose() {
        if (recorder != null)
            recorder.dispose();
        if (audioDevice != null)
            audioDevice.dispose();
    }

    private void pause() {
        paused = true;
    }

    private void resume() {
        paused = false;
    }

    public boolean isRecording() {
        return recording;
    }

    public float[] getSpectrum() {
        return spectrum;
    }
}
