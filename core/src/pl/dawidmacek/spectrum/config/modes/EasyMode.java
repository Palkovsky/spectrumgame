package pl.dawidmacek.spectrum.config.modes;



public class EasyMode extends GameMode{

    @Override
    public float xSpeed() {
        return 8f;
    }

    @Override
    public float minTime() {
        return .5f;
    }

    @Override
    public float maxTime() {
        return .8f;
    }

    @Override
    public float accuracyLimiter() {
        return 75;
    }

    @Override
    public boolean allowEasingChange() {
        return false;
    }


}
