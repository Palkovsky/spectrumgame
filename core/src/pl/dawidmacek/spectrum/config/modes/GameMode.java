package pl.dawidmacek.spectrum.config.modes;

public abstract class GameMode {

    public abstract float xSpeed();

    public abstract float minTime();

    public abstract float maxTime();

    public abstract float accuracyLimiter();

    public boolean allowEasingChange(){
        return true;
    };

}
