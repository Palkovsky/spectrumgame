package pl.dawidmacek.spectrum.config.modes;



public class HardMode extends GameMode{

    @Override
    public float xSpeed() {
        return 20f;
    }

    @Override
    public float minTime() {
        return .1f;
    }

    @Override
    public float maxTime() {
        return .2f;
    }

    @Override
    public float accuracyLimiter() {
        return 25;
    }
}
