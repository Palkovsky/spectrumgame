package pl.dawidmacek.spectrum.config.modes;


public class NormalMode extends GameMode{

    @Override
    public float xSpeed() {
        return  55f;
    }

    @Override
    public float minTime() {
        return .3f;
    }

    @Override
    public float maxTime() {
        return .4f;
    }

    @Override
    public float accuracyLimiter() {
        return 280;
    }
}