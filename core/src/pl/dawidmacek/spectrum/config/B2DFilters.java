package pl.dawidmacek.spectrum.config;


public class B2DFilters {
    public static final short BIT_HEAD = 2;
    public static final short BIT_GROUND = 4;
    public static final short BIT_SEGMENT = 8;
}
