package pl.dawidmacek.spectrum.models;

import java.io.Serializable;

import pl.dawidmacek.spectrum.utils.Player;

public class AudioFile implements Serializable {

    private String title;
    private String artist;
    private String path;
    private String displayName;
    private long duration;

    public AudioFile(String title, String artist, String path, String displayName, long duration) {
        this.title = title;
        this.artist = artist;
        this.path = path;
        this.displayName = displayName;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public boolean isValid() {
        return Player.getInstance().isValidFile(this.path, false);
    }
}
