package pl.dawidmacek.spectrum.pcmgdx.wrapper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.AudioDevice;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;

import net.sourceforge.jaad.aac.AACException;

import pl.dawidmacek.spectrum.pcmgdx.decoders.AACDecoder;
import pl.dawidmacek.spectrum.pcmgdx.decoders.AudioDecoder;
import pl.dawidmacek.spectrum.pcmgdx.decoders.FlacDecoder;
import pl.dawidmacek.spectrum.pcmgdx.decoders.M4ADecoder;
import pl.dawidmacek.spectrum.pcmgdx.decoders.Mpeg3Decoder;
import pl.dawidmacek.spectrum.pcmgdx.decoders.OggDecoder;
import pl.dawidmacek.spectrum.pcmgdx.decoders.SampleFrame;
import pl.dawidmacek.spectrum.pcmgdx.decoders.WAVDecoder;

public class PCMPlayer {

    public enum PlayerState {PLAYING, PAUSED, FINISHED, SEEKING, DISPOSED}

    public enum FileType {MP3, FLAC, WAV, OGG, AIFF, M4A, AAC}

    private AudioDecoder decoder;
    private AudioDevice device;
    private Thread playbackThread;

    private PlayerState state;
    private PlayerState preSeekState;
    private PCMPlayerFrameListener frameListener;
    private PCMPlayerStateChangeListener stateListener;

    private float volume;
    private float seekTarget;
    private boolean quietMode; //used when you want only to get your PCM frames.
    private boolean shouldReset;

    public PCMPlayer(FileHandle file) {
        this(file, false, findFileType(file.extension()));
    }

    public PCMPlayer(FileHandle file, boolean quietMode) {
        this(file, quietMode, findFileType(file.extension()));
    }

    public PCMPlayer(FileHandle file, boolean quietMode, FileType fileType) throws GdxRuntimeException {
        this.quietMode = quietMode;
        this.volume = 1;

        this.decoder = findDecoder(file, fileType);

        if (this.decoder == null || this.decoder.getFrequency() == 0 || this.decoder.getChannels() == 0)
            throw new GdxRuntimeException("Audio file couldn't be played.");

        if (!this.quietMode)
            this.device = Gdx.audio.newAudioDevice(this.decoder.getFrequency(), this.decoder.getChannels() == 1);

        this.playbackThread = new Thread(new Runnable() {
            @Override
            public void run() {

                // System.out.println(decoder.getFrequency() + " Hz, " + decoder.getSampleSize() + " bits, " + decoder.getChannels() + " channels");

                while (!Thread.currentThread().isInterrupted()) {

                    //System.out.println(decoder.getPosition());
                    //System.out.println(state);

                    if (shouldReset) {
                        decoder.reset();
                        shouldReset = false;
                    }

                    if (state == PlayerState.SEEKING) {

                        decoder.setPosition(seekTarget);
                        seekTarget = 0;
                        setState(preSeekState);
                        preSeekState = null;

                    } else if (state == PlayerState.PLAYING) {

                        SampleFrame frame = decoder.nextFrame();

                        if (frame == null) {
                            setState(PlayerState.FINISHED);
                        } else {
                            if (frameListener != null)
                                frameListener.onNewFrame(frame);
                            if (!PCMPlayer.this.quietMode) {
                                try {
                                    device.writeSamples(frame.getData(), 0, frame.getLength());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    break;
                                }
                            }
                        }

                    }

                }
            }
        });

        /*
        Gdx.app.addLifecycleListener(new LifecycleListener() {
            @Override
            public void pause() {
                PCMPlayer.this.pause();
            }

            @Override
            public void resume() {
                if (seekTarget != 0)
                    setState(PlayerState.SEEKING);
                else if (state == PlayerState.PAUSED) {
                    PCMPlayer.this.unpause();
                }

            }

            @Override
            public void dispose() {
                PCMPlayer.this.dispose();
            }
        });
        */

        playbackThread.start();
    }


    public void dispose() {
        if (!playbackThread.isInterrupted())
            playbackThread.interrupt();
        if (device != null) {
            device.dispose();
        }
        playbackThread = null;
        decoder = null;
        frameListener = null;
        stateListener = null;
        setState(PlayerState.DISPOSED);
    }

    private void setState(PlayerState targetState) {
        if (state != PlayerState.DISPOSED && targetState != state) {
            if (stateListener != null)
                stateListener.onStateChange(state, targetState);
            this.state = targetState;
        }
    }

    public boolean isLooping() {
        return decoder.isLooping();
    }

    public void setLooping(boolean looping) {
        decoder.setLooping(looping);
    }

    public float getProgress() {
        return decoder.getPosition();
    }

    public void setProgress(float seconds) {
        preSeekState = state;
        setState(PlayerState.SEEKING);
        seekTarget = seconds;
        while(seekTarget != 0){};
    }

    public void reset() {
        shouldReset = true;
        seekTarget = 0;
        preSeekState = null;
    }

    public void pause() {
        setState(PlayerState.PAUSED);
    }

    public void unpause() {
        if (state == PlayerState.PAUSED)
            setState(PlayerState.PLAYING);
    }

    public void play() {
        if (state != PlayerState.PLAYING && state != PlayerState.SEEKING) {
            setState(PlayerState.PLAYING);
        }
        if(state == PlayerState.SEEKING)
            preSeekState = PlayerState.PLAYING;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
        device.setVolume(volume);
    }

    public PlayerState getState() {
        return state;
    }

    public void setFrameListener(PCMPlayerFrameListener frameListener) {
        this.frameListener = frameListener;
    }

    public void setStateListener(PCMPlayerStateChangeListener stateListener) {
        this.stateListener = stateListener;
    }

    private static AudioDecoder findDecoder(FileHandle file, FileType fileType) throws GdxRuntimeException {
        switch (fileType) {
            case MP3:
                return new Mpeg3Decoder(file);
            case FLAC:
                return new FlacDecoder(file);
            case OGG:
                return new OggDecoder(file);
            case M4A:
                try {
                    return new M4ADecoder(file);
                } catch (AACException e) {
                    throw new GdxRuntimeException("Incompatible m4a file.");
                }
            case AAC:
                return new AACDecoder(file);
            case WAV:
                return new WAVDecoder(file);
            case AIFF:
                return new AACDecoder(file);
        }

        throw new GdxRuntimeException("Incompatible audio file.");
    }

    private static FileType findFileType(String ext) {
        if (ext.equals("mp3"))
            return FileType.MP3;
        if (ext.equals("flac"))
            return FileType.FLAC;
        if (ext.equals("ogg"))
            return FileType.OGG;
        if (ext.equals("m4a"))
            return FileType.M4A;
        if (ext.equals("aac"))
            return FileType.AAC;
        if (ext.equals("wav"))
            return FileType.WAV;
        if (ext.equals("aiff"))
            return FileType.AIFF;
        return null;
    }

    public interface PCMPlayerStateChangeListener {
        void onStateChange(PlayerState previousState, PlayerState currentState);
    }

    public interface PCMPlayerFrameListener {
        void onNewFrame(SampleFrame frame);
    }
}
