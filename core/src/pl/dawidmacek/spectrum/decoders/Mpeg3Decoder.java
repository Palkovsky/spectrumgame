package pl.dawidmacek.spectrum.decoders;

import java.io.InputStream;

import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Decoder;
import javazoom.jl.decoder.DecoderException;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.SampleBuffer;


public class Mpeg3Decoder extends AudioDecoder {

    private Bitstream bitstream;
    private Decoder decoder;

    private int frequency, chanel, size;

    public Mpeg3Decoder(InputStream inputStream) {
        super(inputStream);

        bitstream = new Bitstream(inputStream);
        decoder = new Decoder();
        try {
            Header nextFrame = bitstream.readFrame();
            if (nextFrame != null) {
                decoder.decodeFrame(nextFrame, bitstream);
                frequency = decoder.getOutputFrequency();
                chanel = decoder.getOutputChannels();
                size = decoder.getOutputBlockSize();

                bitstream = new Bitstream(inputStream);
                decoder = new Decoder();
            }
        } catch (BitstreamException e) {
            e.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        }
    }

    @Override
    public SampleFrame nextFrame() {
        try {
            Header frameHeader = bitstream.readFrame();

            if (decoder == null || bitstream == null || frameHeader == null)
                return null;

            SampleBuffer buffer = null;
            try {
                buffer = ((SampleBuffer) decoder.decodeFrame(frameHeader, bitstream));
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
            if (buffer == null)
                return null;

            short[] samples = buffer.getBuffer();
            bitstream.closeFrame();
            return new SampleFrame(samples, buffer.getBufferLength());

        } catch (BitstreamException e) {
            e.printStackTrace();
        } catch (DecoderException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getFrequency() {
        return frequency;
    }

    @Override
    public int getChannels() {
        return chanel;
    }

    @Override
    public int getSampleSize() {
        return size;
    }

    @Override
    public boolean isBigEndian() {
        return false;
    }

}
