package pl.dawidmacek.spectrum.decoders;

import java.io.InputStream;

public abstract class AudioDecoder {

    protected InputStream inputStream;

    public AudioDecoder(InputStream inputStream){
        this.inputStream = inputStream;
    }

    public abstract SampleFrame nextFrame();

    public abstract int getFrequency();

    public abstract int getChannels();

    public abstract int getSampleSize();

    public abstract boolean isBigEndian();
}