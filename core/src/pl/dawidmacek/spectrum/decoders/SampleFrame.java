package pl.dawidmacek.spectrum.decoders;

public class SampleFrame {
    private short[] data;
    private int length;

    public SampleFrame(short[] data, int length) {
        this.data = data;
        this.length = length;
    }

    public short[] getData() {
        return data;
    }

    public int getLength() {
        return length;
    }
}
