package pl.dawidmacek.spectrum.decoders;

import org.kc7bfi.jflac.FLACDecoder;
import org.kc7bfi.jflac.frame.Frame;
import org.kc7bfi.jflac.util.ByteData;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class FlacDecoder extends AudioDecoder {

    private FLACDecoder decoder;

    public FlacDecoder(InputStream inputStream) {
        super(inputStream);
        decoder = new FLACDecoder(inputStream);
        try {
            decoder.readMetadata();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public SampleFrame nextFrame() {

        try {
            Frame frame = decoder.readNextFrame();

            if (frame != null) {
                ByteData buffer = new ByteData(frame.header.bitsPerSample);
                ByteData decoded = decoder.decodeFrame(frame, buffer);

                byte[] audio = decoded.getData();
                short[] shortSamples = new short[audio.length / 2];
                if (isBigEndian()) {
                    ByteBuffer.wrap(audio).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(shortSamples);
                } else {
                    ByteBuffer.wrap(audio).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shortSamples);
                }

                return new SampleFrame(shortSamples, decoded.getLen()/2);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getFrequency() {
        return decoder.getStreamInfo().getSampleRate();
    }

    @Override
    public int getChannels() {
        return decoder.getStreamInfo().getChannels();
    }

    @Override
    public int getSampleSize() {
        return decoder.getStreamInfo().getBitsPerSample();
    }

    @Override
    public boolean isBigEndian() {
        return false;
    }
}