package pl.dawidmacek.spectrum.decoders;

import net.sourceforge.jaad.aac.ChannelConfiguration;
import net.sourceforge.jaad.aac.Decoder;
import net.sourceforge.jaad.aac.SampleBuffer;
import net.sourceforge.jaad.aac.SampleFrequency;
import net.sourceforge.jaad.mp4.MP4Container;
import net.sourceforge.jaad.mp4.api.Frame;
import net.sourceforge.jaad.mp4.api.Track;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class M4ADecoder extends AudioDecoder {

    private Track track;
    private Decoder decoder;
    private SampleBuffer buffer;

    public M4ADecoder(InputStream inputStream) {
        super(inputStream);
        try {
            MP4Container mp4Container = new MP4Container(inputStream);
            track = mp4Container.getMovie().getTracks().get(0);
            decoder = new Decoder(track.getDecoderSpecificInfo());
            decoder.getConfig().setSampleFrequency(SampleFrequency.SAMPLE_FREQUENCY_44100);
            decoder.getConfig().setChannelConfiguration(ChannelConfiguration.CHANNEL_CONFIG_STEREO);
            buffer = new SampleBuffer();
            buffer.setBigEndian(true);
            System.out.println(decoder.getConfig().getSampleFrequency().getFrequency());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public SampleFrame nextFrame() {

        try {

            Frame nextFrame = track.readNextFrame();

            if(nextFrame != null) {
                byte[] bytes = nextFrame.getData();
                decoder.decodeFrame(bytes, buffer);
                byte[] audio = buffer.getData();

                short[] shortSamples = new short[audio.length / 2];
                if (isBigEndian()) {
                    ByteBuffer.wrap(audio).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(shortSamples);
                } else {
                    ByteBuffer.wrap(audio).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(shortSamples);
                }

                return new SampleFrame(shortSamples, shortSamples.length);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    public int getFrequency() {
        return decoder.getConfig().getSampleFrequency().getFrequency();
    }

    @Override
    public int getChannels() {
        return buffer.getChannels();
    }

    @Override
    public int getSampleSize() {
        return buffer.getBitsPerSample();
    }

    @Override
    public boolean isBigEndian() {
        return buffer.isBigEndian();
    }

}
