package pl.dawidmacek.spectrum.ui.tweens;

import com.badlogic.gdx.math.Vector2;

import aurelienribon.tweenengine.TweenAccessor;

public class Vector2Tween implements TweenAccessor<Vector2> {

    public static final int POS_XY = 1;
    public static final int POS_X = 2;
    public static final int POS_Y = 3;


    @Override
    public int getValues(Vector2 target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case POS_XY:
                returnValues[0] = target.x;
                returnValues[1] = target.y;
                return 2;
            case POS_X:
                returnValues[0] = target.x;
                return 1;
            case POS_Y:
                returnValues[0] = target.y;
                return 1;
            default:
                return -1;
        }
    }

    @Override
    public void setValues(Vector2 target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case POS_XY:
                target.set(newValues[0], newValues[1]);
                break;
            case POS_X:
                target.x = newValues[0];
                break;
            case POS_Y:
                target.y = newValues[0];
                break;
            default:
        }
    }
}