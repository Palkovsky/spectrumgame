package pl.dawidmacek.spectrum.ui.tweens;


import aurelienribon.tweenengine.TweenAccessor;
import pl.dawidmacek.spectrum.objects.notes.SlideNote;

public class SlideNoteTween implements TweenAccessor<SlideNote> {

    public static final int START_NOTE_RADIUS_OFFSET = 1;
    public static final int END_NOTE_RADIUS_OFFSET = 2;

    @Override
    public int getValues(SlideNote target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case START_NOTE_RADIUS_OFFSET:
                returnValues[0] = target.getStartNoteRadiusOffset();
                return 1;
            case END_NOTE_RADIUS_OFFSET:
                returnValues[0] = target.getEndNoteRadiusOffset();
                return 1;
            default:
                return 0;
        }
    }

    @Override
    public void setValues(SlideNote target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case START_NOTE_RADIUS_OFFSET:
                target.setStartNoteRadiusOffset(newValues[0]);
                break;
            case END_NOTE_RADIUS_OFFSET:
                target.setEndNoteRadiusOffset(newValues[0]);
                break;
        }
    }

}
