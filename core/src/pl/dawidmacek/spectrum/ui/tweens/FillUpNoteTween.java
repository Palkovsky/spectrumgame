package pl.dawidmacek.spectrum.ui.tweens;


import aurelienribon.tweenengine.TweenAccessor;
import pl.dawidmacek.spectrum.objects.notes.FillUpNote;

public class FillUpNoteTween implements TweenAccessor<FillUpNote> {

    public static final int FILL = 1;
    public static final int SIZE_OFFSET = 2;

    @Override
    public int getValues(FillUpNote target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case FILL:
                returnValues[0] = target.getCurrentFill();
                return 1;
            case SIZE_OFFSET:
                returnValues[0] = target.getSizeOffset();
                return 1;
            default:
                return 0;
        }
    }

    @Override
    public void setValues(FillUpNote target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case FILL:
                target.setCurrentFill(newValues[0]);
                break;
            case SIZE_OFFSET:
                target.setSizeOffset(newValues[0]);
                break;
        }
    }
}
