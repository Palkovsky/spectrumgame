package pl.dawidmacek.spectrum.ui.tweens;


import aurelienribon.tweenengine.TweenAccessor;
import pl.dawidmacek.spectrum.objects.ProgressBar;

public class ProgressBarTween implements TweenAccessor<ProgressBar> {

    public static final int PROGRESS = 1;

    @Override
    public int getValues(ProgressBar target, int tweenType, float[] returnValues) {
        switch (tweenType) {
            case PROGRESS:
                returnValues[0] = target.getProgress();
                return 1;
            default:
                return -1;
        }
    }

    @Override
    public void setValues(ProgressBar target, int tweenType, float[] newValues) {
        switch (tweenType) {
            case PROGRESS:
                target.setProgress(newValues[0]);
                break;
        }
    }
}
