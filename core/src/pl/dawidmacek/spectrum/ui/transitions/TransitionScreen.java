package pl.dawidmacek.spectrum.ui.transitions;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import aurelienribon.tweenengine.TweenManager;

public abstract class TransitionScreen implements Screen {

    //Viewport
    protected Viewport viewport;

    //Cameras
    protected OrthographicCamera cam;

    protected SpriteBatch spriteBatch;
    protected TweenManager manager;

    private float width, height;
    private float duration;

    protected Screen currentScreen, nextScreen;
    protected Sprite currentSprite, nextSprite;
    protected Sprite copyCurrentSprite, copyNextSprite;
    protected FrameBuffer currentBuffer, nextBuffer;
    protected Game game;

    public TransitionScreen(Game game, Screen currentScreen, Screen nextScreen, float width, float height, float duration) {
        this.game = game;
        this.currentScreen = currentScreen;
        this.nextScreen = nextScreen;
        this.width = width;
        this.height = height;
        this.duration = duration;
    }

    @Override
    public void show() {
        spriteBatch = new SpriteBatch();
        manager = new TweenManager();

        cam = new OrthographicCamera();
        cam.setToOrtho(false, width, height);
        viewport = new StretchViewport(width, height, cam);
        cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);

        nextBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
        nextBuffer.begin();
        nextScreen.render(Gdx.graphics.getDeltaTime());
        nextBuffer.end();

        nextSprite = new Sprite(nextBuffer.getColorBufferTexture());
        nextSprite.setPosition(0, 0);
        nextSprite.flip(false, true);
        copyNextSprite = nextSprite;

        currentBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
        currentBuffer.begin();
        currentScreen.render(Gdx.graphics.getDeltaTime());
        currentBuffer.end();

        currentSprite = new Sprite(currentBuffer.getColorBufferTexture());
        currentSprite.setPosition(0, 0);
        currentSprite.flip(false, true);
        copyCurrentSprite = currentSprite;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        viewport.apply();
        cam.update();
        spriteBatch.setProjectionMatrix(cam.combined);
        manager.update(delta);
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);
    }


    @Override
    public void dispose() {
        spriteBatch.dispose();
        currentSprite.getTexture().dispose();
        nextSprite.getTexture().dispose();
        copyCurrentSprite.getTexture().dispose();
        copyNextSprite.getTexture().dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getDuration() {
        return duration;
    }
}