package pl.dawidmacek.spectrum.ui.transitions;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Quad;
import pl.dawidmacek.spectrum.screens.BaseScreen;
import pl.dawidmacek.spectrum.ui.tweens.SpriteTween;


public class FadeTransitionScreen extends TransitionScreen {

    private boolean animatingFirst;
    private Sprite toDraw;

    public FadeTransitionScreen(Game game, BaseScreen currentScreen, BaseScreen nextScreen, float width, float height, float duration) {
        super(game, currentScreen, nextScreen, width, height, duration);
        ((BaseScreen) this.currentScreen).disableBackKey();
        ((BaseScreen) this.nextScreen).disableBackKey();
        ((BaseScreen) this.currentScreen).disableInput();
        ((BaseScreen) this.nextScreen).disableInput();
    }


    @Override
    public void show() {
        super.show();
        toDraw = copyCurrentSprite;
        animatingFirst = true;

        final TweenCallback backgroundAnimationTweenComplete = new TweenCallback() {
            @Override
            public void onEvent(int type, BaseTween<?> source) {
                game.setScreen(nextScreen);
                ((BaseScreen) currentScreen).enableBackKey();
                ((BaseScreen) nextScreen).enableBackKey();
                ((BaseScreen) currentScreen).enableInput();
                ((BaseScreen) nextScreen).enableInput();
            }
        };

        Tween.set(nextSprite, SpriteTween.ALPHA).target(0).start(manager);
        Tween.set(currentSprite, SpriteTween.ALPHA).target(1).start(manager);

        Tween.to(currentSprite, SpriteTween.ALPHA, getDuration() / 1.5f).target(0).setCallback(new TweenCallback() {
            @Override
            public void onEvent(int type, BaseTween<?> source) {
                animatingFirst = false;
                Tween.to(nextSprite, SpriteTween.ALPHA, getDuration() - (getDuration() / 1.5f))
                        .target(1)
                        .ease(Quad.IN)
                        .setCallback(backgroundAnimationTweenComplete)
                        .setCallbackTriggers(TweenCallback.COMPLETE)
                        .start(manager);
            }
        }).ease(Quad.OUT)
                .start(manager);

        currentScreen.show();
        nextScreen.show();
    }


    @Override
    public void render(float delta) {
        super.render(delta);
        if (animatingFirst) {
            currentBuffer.begin();
            currentScreen.render(delta);
            currentBuffer.end();

            Color currentColor = currentSprite.getColor();
            copyCurrentSprite = new Sprite(currentBuffer.getColorBufferTexture());
            copyCurrentSprite.setPosition(0, 0);
            copyCurrentSprite.flip(false, true);

            copyCurrentSprite.setColor(currentColor);

            toDraw = copyCurrentSprite;

        } else {
            nextBuffer.begin();
            nextScreen.render(delta);
            nextBuffer.end();

            Color nextColor = nextSprite.getColor();
            copyNextSprite = new Sprite(nextBuffer.getColorBufferTexture());
            copyNextSprite.setPosition(0, 0);
            copyNextSprite.flip(false, true);
            copyNextSprite.setColor(nextColor);

            toDraw = copyNextSprite;
        }

        spriteBatch.begin();
        toDraw.setSize(getWidth(), getHeight());
        toDraw.draw(spriteBatch);
        spriteBatch.end();
    }

    public void animateFirst(boolean animateFirst) {
        this.animatingFirst = animateFirst;
    }
}
