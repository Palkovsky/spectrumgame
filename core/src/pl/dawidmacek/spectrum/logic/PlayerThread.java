package pl.dawidmacek.spectrum.logic;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import java.util.Arrays;

import edu.emory.mathcs.jtransforms.fft.FloatFFT_1D;

class PlayerThread extends Thread implements Disposable {

    private static final int FRAMES_READ = 1;

    private AudioPlayer owner;
    private Array<Float> samples;

    private boolean playing = false;
    private boolean audioEnded = false;

    private FloatFFT_1D fft;
    private float[] audioSpectrum;

    public PlayerThread(AudioPlayer owner) {
        this.fft = new FloatFFT_1D(32);
        this.owner = owner;
        this.samples = new Array<Float>();
    }


    public boolean isPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public boolean hasAudioEnded() {
        return audioEnded;
    }


    @Override
    public void start() {
        playing = true;
        super.start();
    }

    @Override
    public void run() {
        while (!audioEnded) {
            if (playing) {
                owner.decoder.readFrames(samples, PlayerThread.FRAMES_READ);

                if (samples.size == 0) {
                    audioEnded = true;
                    playing = false;
                } else {

                    float[] buffer = new float[samples.size];
                    for (int n = 0; n < buffer.length; n++) {
                        buffer[n] = samples.get(n);
                    }

                    float[] sampl = new float[samples.size];
                    for (int i = 0; i < samples.size; i++) sampl[i] = samples.get(i);
                    audioSpectrum = sampl;
                    fft.complexForward(sampl);
                    owner.device.writeSamples(buffer, 0, buffer.length);
                }
            }
        }
        System.out.println("END");
    }

    public float[] getSpectrum(int bars) {
        if (audioSpectrum != null) {
            int chunkSize = audioSpectrum.length / bars;
            int counter = 0;
            int len = audioSpectrum.length;
            float chunks[][] = new float[bars][chunkSize];
            float avgs[] = new float[bars];

            for (int i = 0; i < len - chunkSize + 1; i += chunkSize)
                chunks[counter++] = Arrays.copyOfRange(audioSpectrum, i, i + chunkSize);

            for (int i = 0; i < chunks.length; i++) {
                float[] chunk = chunks[i];
                float avg = 0;
                for (float item : chunk)
                    avg += item;
                avg = avg / chunk.length;
                avgs[i] = avg * 50000;//Math.abs(avg);
            }


            //sort asc, then change it for desc
            Arrays.sort(avgs);
            for (int i = 0; i < avgs.length / 2; ++i) {
                float temp = avgs[i];
                avgs[i] = avgs[avgs.length - i - 1];
                avgs[avgs.length - i - 1] = temp;
            }


            return avgs;
        }

        return new float[0];
    }


    @Override
    public void dispose() {
        audioEnded = true;
        try {
            this.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}