package pl.dawidmacek.spectrum.handlers;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.AudioRecorder;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.drawing.SpectrumBackground;
import pl.dawidmacek.spectrum.models.AudioFile;
import pl.dawidmacek.spectrum.objects.notes.BaseNote;
import pl.dawidmacek.spectrum.objects.Board;
import pl.dawidmacek.spectrum.objects.Cursor;
import pl.dawidmacek.spectrum.objects.Messager;
import pl.dawidmacek.spectrum.objects.notes.FillUpNote;
import pl.dawidmacek.spectrum.objects.notes.Note;
import pl.dawidmacek.spectrum.objects.ProgressBar;
import pl.dawidmacek.spectrum.objects.notes.SlideNote;
import pl.dawidmacek.spectrum.utils.ColorUtils;
import pl.dawidmacek.spectrum.utils.Converter;
import pl.dawidmacek.spectrum.utils.NumberUtils;
import pl.dawidmacek.spectrum.utils.Player;
import pl.dawidmacek.spectrum.utils.Points;
import pl.dawidmacek.spectrum.utils.RandomUtil;
import pl.dawidmacek.spectrum.utils.Recorder;
import pl.dawidmacek.spectrum.utils.Vertices;

public class PlayManager {

    //Config
    private static final int SPECTRUM_HISTORY_ELEMENTS = 60;
    private static final int SPECTRUM_HISTORY_CHUNKS = 2;
    private static final int NB_BARS = 50;
    private static final int NOTE_LIFESPAN = 60;
    private static final int NEW_NOTE_MIN_OFFSET = 16;
    private static final int PREV_MESSAGE_TIME_OFFSET = 420;
    private static final String[] NEGATIVE_MESSAGES = {
            "WEAK", "YOU'RE PRETTY BAD", "WRONG", "WRONG MOVE", "GO PLAY MY LITTLE PONY",
            "MISERABLE", "POORLY", "TERRIBLE", "JUST AWFUL"
    };
    private static final String[] NEGATIVE_MESSAGES_SKIPPED = {
            "PAY ATTENTION!!!", "WAKE UP!", "MISSED", "ARE YOU BLIND?", "BUCKLE UP!", "DID YOU NOT SEE THAT?"
    };
    private static final String[] POSITIVE_MESSAGES = {
            "GREAT TIMING", "YOU GOT IT", "WELL DONE!", "KEEP IT UP", "ALRIGHT", "KEEP GOING", "THAT'S IT",
            "SYNCHRONIZED", "ACCEPTABLE", "GOOD WORK", "NICE"
    };
    private static final String[] PERFECT_MESSAGES = {
            "THE MASTER!", "PERFECT", "FANTASTIC", "AWESOME", "THE BEST", "SAVAGE", "KICK ASS"
    };

    //Particles
    private static final Sprite circleParticle = new Sprite(new Texture(Gdx.files.internal("particles/circle.png")));
    private static final Sprite squareParticle = new Sprite(new Texture(Gdx.files.internal("particles/square.png")));

    //Objects
    private SpectrumBackground background;
    //private PixmapBackground pixmapBackground;
    private List<BaseNote> notes;
    private SlideNote currentlySlidingNote;
    private Board board;
    private Cursor playerCursor;
    private Cursor pointerCursor;
    private BaseNote prevNote;
    private Messager messager;
    private ProgressBar audioProgressBar;

    //Rendering
    private ParticleEffectPool particleEffectPool;
    private List<ParticleEffectPool.PooledEffect> currentEffects;

    //Data
    private AudioFile audioFile;
    private int historyChunkIndex;
    private int historyElementIndex;
    private float maxChange;
    private float prevChange;
    private float changeAverage;
    private float prevSlideNoteAngle;
    private float[][] spectrumHistory = new float[SPECTRUM_HISTORY_ELEMENTS][SPECTRUM_HISTORY_CHUNKS];
    private Vector2 prevNotePosition;
    private int noteCounter;
    private Color prevNoteColor;
    private List<Board.Cell> unavailableCells;
    private int prevSkip;
    private Note.NoteShape currentlyEmittedShape;
    private int emissionInSecond, hitsInSecond;
    private LinkedList<Integer> hitsHistory, emissionHistory;
    private LinkedList<Float> averageHistory;
    private float currentAvg;
    private boolean microphoneMode;

    //Data for messages
    private int successfulNotes;
    private int passedNotes;

    //User data
    private int score;
    private int currentStreak;

    //Counters
    private int changeAverageRestartCounter;
    private int newNoteCounter;
    private int prevMessageCounter;
    private int speedCounter;
    private int averageCheckCounter;

    //LibGdx objects
    private OrthographicCamera camera;
    private AudioRecorder recorder;

    //Utils
    private RandomUtil rand;

    public PlayManager(OrthographicCamera camera, AudioFile audioFile) {
        this.camera = camera;
        this.audioFile = audioFile;
        this.microphoneMode = audioFile == null;

        if (this.microphoneMode) {
            this.recorder = Gdx.audio.newAudioRecorder(44100, false);
        }

        this.historyChunkIndex = this.historyElementIndex = 0;
        this.prevChange = 0;
        this.maxChange = 1500;
        this.changeAverage = 0;

        this.changeAverageRestartCounter = 1;
        this.newNoteCounter = 0;
        this.prevMessageCounter = 0;
        this.prevSkip = 0;
        this.averageCheckCounter = 0;
        this.currentlyEmittedShape = Note.NoteShape.CIRCLE;
        this.emissionInSecond = this.hitsInSecond = 0;
        this.hitsHistory = new LinkedList<Integer>();
        this.emissionHistory = new LinkedList<Integer>();
        this.averageHistory = new LinkedList<Float>();

        this.successfulNotes = this.passedNotes = 0;

        this.rand = new RandomUtil();

        ParticleEffect effect = new ParticleEffect();
        effect.load(Gdx.files.internal("particles/sparkling.p"), Gdx.files.internal("particles"));
        this.particleEffectPool = new ParticleEffectPool(effect, 25, 25);
        this.currentEffects = new ArrayList<ParticleEffectPool.PooledEffect>();

        notes = new ArrayList<BaseNote>();
        unavailableCells = new ArrayList<Board.Cell>();
        board = new Board((Dimen.WIDTH - Board.WIDTH) / 2, (Dimen.HEIGHT - Board.HEIGHT) / 2);
        playerCursor = new Cursor(ColorUtils.parseRGB("ff6d00"));
        pointerCursor = new Cursor(ColorUtils.parseRGB("2962ff"));
        messager = new Messager();

        Color progressBarBackground = new Color(Color.GRAY);
        progressBarBackground.a = .6f;
        audioProgressBar = new ProgressBar(0, 0, Dimen.WIDTH, 40, progressBarBackground, Color.FIREBRICK);
    }


    public void show() {
        playNew();
    }

    public void update(float delta) {

        if (microphoneMode) {
            Player.getInstance().setSpectrum(Recorder.getInstance().getSpectrum());
        }

        float successRatio = (passedNotes == 0) ? 0 : (successfulNotes * 1f) / passedNotes;

        unavailableCells.clear();
        for (BaseNote note : notes) {

            if (note instanceof Note) {
                Note simpleNote = (Note) note;
                unavailableCells.add(board.getCell(simpleNote.getPosition()));
            } else if (note instanceof SlideNote) {
                SlideNote slideNote = (SlideNote) note;
                unavailableCells.add(board.getCell(slideNote.getStartPosition()));
                unavailableCells.add(board.getCell(slideNote.getEndPosition()));

            } else if (note instanceof FillUpNote) {
                FillUpNote fillUpNote = (FillUpNote) note;
                if (fillUpNote.getCurrentFill() == fillUpNote.getCapacity()) {
                    emmitParticles(fillUpNote.getPosition(), Note.NoteShape.SQUARE, new Color[]{fillUpNote.getNoteColor(), fillUpNote.getRingColor()});
                    fillUpNote.finish();
                    score();
                }
            }
        }
        board.setUnavailableCells(unavailableCells);

        background.setCurrentStreak(currentStreak);
        background.setScore(score);

        analyzeSpectrum();

        pointerCursor.update(delta);
        playerCursor.update(delta);
        board.update(delta);

        Iterator<BaseNote> baseNoteIterator = notes.iterator();
        while (baseNoteIterator.hasNext()) {
            BaseNote baseNote = baseNoteIterator.next();
            baseNote.update(delta);

            if (baseNote.hasEnded() && !baseNote.isFinishing()) {
                baseNote.finish();
                interruptStreak();
                sendNegativeMessage(true);
            } else if (baseNote.isFinishing()) {
                baseNoteIterator.remove();
            }
        }

        if (audioFile != null) {
            float progress = (Player.getInstance().getProgress() / (audioFile.getDuration() / 1000f)) * 100f;
            progress = (progress > 100) ? 100 : progress;
            audioProgressBar.animateProgress(progress);
        }

        if (notes.size() == 0 && newNoteCounter > 5) {
            noteCounter = 0;
        }

        messager.update(delta);
        prevMessageCounter++;

        if (speedCounter > 60) {
            hitsHistory.add(hitsInSecond);
            emissionHistory.add(emissionInSecond);
            speedCounter = 0;
            hitsInSecond = emissionInSecond = 0;

            if (hitsHistory.size() > 3) {
                int total = 0;
                for (int i = hitsHistory.size() - 1; i >= hitsHistory.size() - 3; i--) {
                    total += hitsHistory.get(i);
                }
                if (total > 11 && currentStreak >= 11) {
                    sendMessage("SO FAST!", true);
                    hitsHistory.clear();
                }
            }
        }
        speedCounter++;

        if (averageCheckCounter > 70) {
            if (currentAvg > changeAverage * 8) {
                averageHistory.add(changeAverage);
            }
            averageCheckCounter = 0;
        }
        averageCheckCounter++;
    }

    public void render(PolygonSpriteBatch spriteBatch) {
        board.render(spriteBatch);

        Iterator<ParticleEffectPool.PooledEffect> pooledEffectsIterator = currentEffects.iterator();
        while (pooledEffectsIterator.hasNext()) {
            ParticleEffectPool.PooledEffect pooledEffect = pooledEffectsIterator.next();
            pooledEffect.draw(spriteBatch, Gdx.graphics.getDeltaTime());

            if (pooledEffect.isComplete()) {
                pooledEffect.reset();
                pooledEffect.free();
                pooledEffectsIterator.remove();
            }
        }

        for (int i = notes.size() - 1; i >= 0; i--)
            notes.get(i).render(spriteBatch);
        pointerCursor.render(spriteBatch);
        //playerCursor.render(spriteBatch);

        audioProgressBar.render(spriteBatch);
        messager.render(spriteBatch);
    }

    public void renderBackground(PolygonSpriteBatch spriteBatch) {
        background.render(spriteBatch);
    }

    public void dispose() {
        if (recorder != null)
            recorder.dispose();
    }

    public void handleInput() {
        if (Gdx.input.isTouched()) {

            Vector2 touch = Converter.convertCoordinates(camera, Gdx.input.getX(), Gdx.input.getY());
            boolean noteFound = true;

            if (currentlySlidingNote == null) {
                for (BaseNote baseNote : notes) {

                    if (baseNote instanceof SlideNote) {
                        SlideNote slideNote = (SlideNote) baseNote;

                        currentlySlidingNote = (!slideNote.isSliding() && !slideNote.isFinishing()
                                && ((slideNote.isRebound() && slideNote.doesStartNoteContainsPoint(touch)) || (!slideNote.isRebound() && (slideNote.doesStartNoteContainsPoint(touch) || slideNote.doesEndNoteContainsPoint(touch))))) ? slideNote : null;

                        if (currentlySlidingNote != null) {
                            break;
                        }

                    } else if (baseNote instanceof Note) {

                        Note note = (Note) baseNote;

                        if (Gdx.input.justTouched()) {

                            if (note.containsPoint(touch)) {
                                if (!note.isTapped() && !note.isFinishing()) {
                                    emmitParticles(note.getPosition(), note.getShape(), new Color[]{note.getNoteColor(), note.getRingColor()});
                                    note.tap();
                                    playerCursor.goTo(note.getPosition());
                                    score();
                                }
                                break;
                            } else {
                                noteFound = false;
                            }
                        }
                    } else if (baseNote instanceof FillUpNote) {

                        FillUpNote fillUpNote = (FillUpNote) baseNote;

                        if (Gdx.input.justTouched()) {
                            if (fillUpNote.containsPoint(touch)) {
                                if (!fillUpNote.isFinishing()) {
                                    currentStreak++;
                                    fillUpNote.addOne();
                                    playerCursor.goTo(fillUpNote.getPosition());
                                    emmitParticles(fillUpNote.getPosition(), Note.NoteShape.SQUARE, new Color[]{fillUpNote.getNoteColor(), fillUpNote.getRingColor()});

                                }
                            }
                            break;
                        } else {
                            noteFound = false;
                        }
                    }
                }

                if (!noteFound && Gdx.input.justTouched()) {
                    interruptStreak();
                    sendNegativeMessage(false);
                }

            } else {

                currentlySlidingNote.setTouchPosition(touch);

                if (!currentlySlidingNote.isSliding()) {
                    currentlySlidingNote.startSliding(touch);
                }
                if (!currentlySlidingNote.doesStartNoteContainsPoint(touch) && !currentlySlidingNote.doesPathContainsPoint(touch) && !currentlySlidingNote.doesEndNoteContainsPoint(touch)) {

                    currentlySlidingNote.setSlidingSuccess(false);
                    currentlySlidingNote = null;
                    interruptStreak();
                    sendNegativeMessage(false);

                } else if (currentlySlidingNote.doesDestinationNoteContainsPoint(touch)) {

                    if (currentlySlidingNote.isRebound() && !currentlySlidingNote.isReboundReached())
                        currentlySlidingNote.setSlidingSuccess(true);
                    else {
                        emmitParticles(currentlySlidingNote.getStartPosition(), Note.NoteShape.CIRCLE, new Color[]{currentlySlidingNote.getNoteColor(), currentlySlidingNote.getRingColor()});
                        emmitParticles(currentlySlidingNote.getEndPosition(), Note.NoteShape.CIRCLE, new Color[]{currentlySlidingNote.getNoteColor(), currentlySlidingNote.getRingColor()});
                        currentlySlidingNote.setSlidingSuccess(true);
                        currentlySlidingNote = null;
                        score();
                    }
                }
            }

        } else {
            if (currentlySlidingNote != null) {
                currentlySlidingNote.interruptSliding();
            }
            currentlySlidingNote = null;
        }
    }

    private void emmitParticles(Vector2 pos, Note.NoteShape noteShape, Color[] colors) {
        ParticleEffectPool.PooledEffect pooledEffect = particleEffectPool.obtain();
        pooledEffect.setPosition(pos.x, pos.y);

        for (int i = 0; i < pooledEffect.getEmitters().size; i++) {
            ParticleEmitter particleEmitter = pooledEffect.getEmitters().get(i);

            switch (noteShape) {
                case HEXAGON:
                case CIRCLE:
                    particleEmitter.setSprite(circleParticle);
                    break;
                default:
                case SQUARE:
                    particleEmitter.setSprite(squareParticle);
                    break;
            }

            Color color = colors[i % colors.length];
            particleEmitter.getTint().setColors(new float[]{color.r, color.g, color.b, color.a});

            int velocityMultiplier = 0;
            for (BaseNote note : notes) {
                if (note instanceof SlideNote) {
                    velocityMultiplier += 2;
                } else if (note instanceof FillUpNote) {
                    velocityMultiplier += ((FillUpNote) note).getCapacity() - ((FillUpNote) note).getCurrentFill();
                } else {
                    velocityMultiplier += 1;
                }
            }
            velocityMultiplier += (prevSkip / 2);
            velocityMultiplier = (velocityMultiplier == 0) ? 1 : velocityMultiplier;

            if (i == 0) {
                if (noteShape != Note.NoteShape.CIRCLE && noteShape != Note.NoteShape.HEXAGON) {
                    particleEmitter.getVelocity().setHigh(velocityMultiplier * 200, velocityMultiplier * 300);
                    particleEmitter.addParticles(currentStreak);
                } else {
                    particleEmitter.getVelocity().setHigh(velocityMultiplier * 220, velocityMultiplier * 320);
                    particleEmitter.addParticles(currentStreak * 3);
                }
            } else {
                if (noteShape != Note.NoteShape.CIRCLE && noteShape != Note.NoteShape.HEXAGON) {
                    particleEmitter.getVelocity().setHigh(velocityMultiplier * 100, velocityMultiplier * 150);
                    particleEmitter.addParticles(currentStreak / 2);
                } else {
                    particleEmitter.getVelocity().setHigh(velocityMultiplier * 120, velocityMultiplier * 170);
                    particleEmitter.addParticles((int) (currentStreak * 1.5f));
                }
            }

            particleEmitter.getWind().setHigh(currentStreak * 400, currentStreak * 400);
        }

        currentEffects.add(pooledEffect);
    }

    private void interruptStreak() {
        currentStreak = 0;
        Gdx.input.vibrate(100);
        //sendNegativeMessage();
        passedNotes++;
    }

    private void score() {
        currentStreak++;
        score += currentStreak;
        successfulNotes++;
        passedNotes++;
        hitsInSecond++;
        if (currentStreak % 25 == 0)
            sendMessage("x" + currentStreak, background.getBaseColor(), false);
        else
            sendPositiveMessage();
    }

    private void playNew() {
        score = 0;
        currentStreak = 0;
        noteCounter = 0;

        if (microphoneMode) {
            Recorder.getInstance().start();
        } else {
            Player.getInstance().play(audioFile.getPath(), false);
            /*
            float[] vertices_1 = Vertices.arc(Dimen.WIDTH / 4, Dimen.HEIGHT / 2, 250, -120, 180);
            notes.add(new SlideNote(vertices_1[0], vertices_1[1], vertices_1[vertices_1.length - 2], vertices_1[vertices_1.length - 1], 100000, 69, true, vertices_1));
            float[] vertices_2 = Vertices.arc(Dimen.WIDTH / 2 + Dimen.WIDTH / 4, Dimen.HEIGHT / 2, 250, 120, -180);
            notes.add(new SlideNote(vertices_2[0], vertices_2[1], vertices_2[vertices_2.length - 2], vertices_2[vertices_2.length - 1], 100000, 69, true, vertices_2));
            notes.add(new FillUpNote(Dimen.WIDTH / 2, Dimen.HEIGHT / 2, 10000, 69, 5));
            */
        }
        background = new SpectrumBackground();
    }

    private void sendMessage(String text, boolean important) {
        sendMessage(text, Color.PURPLE, important);
    }

    private void sendMessage(String text, Color color, boolean important) {
        messager.send(text, color, important, 1);
    }


    private void sendPositiveMessage() {
        if (prevMessageCounter > PREV_MESSAGE_TIME_OFFSET) {
            if (currentStreak > 50)
                messager.send(PERFECT_MESSAGES[rand.randInt(0, PERFECT_MESSAGES.length - 1)], Color.GOLD, true, 0);
            else
                messager.send(POSITIVE_MESSAGES[rand.randInt(0, POSITIVE_MESSAGES.length - 1)], Color.ROYAL, true, 0);
            prevMessageCounter = 0;
        }
    }

    private void sendNegativeMessage(boolean skipped) {
        if (prevMessageCounter > PREV_MESSAGE_TIME_OFFSET / 3) {
            if (skipped)
                messager.send(NEGATIVE_MESSAGES_SKIPPED[rand.randInt(0, NEGATIVE_MESSAGES_SKIPPED.length - 1)], Color.RED, true, 0);
            else
                messager.send(NEGATIVE_MESSAGES[rand.randInt(0, NEGATIVE_MESSAGES.length - 1)], Color.RED, true, 0);
            prevMessageCounter = 0;
        }
    }

    private void analyzeSpectrum() {
        float[] audioSpectrum = Player.getInstance().getSpectrum(NB_BARS);

        if (audioSpectrum.length >= NB_BARS) {
            float avg = 0;
            for (float v : audioSpectrum)
                avg += v;
            avg /= audioSpectrum.length;
            currentAvg = avg;

            float totalChange = 0;

            for (int i = 1; i < spectrumHistory.length; i++) {

                for (int j = 0; j < spectrumHistory[i].length; j++) {
                    float change = (spectrumHistory[i][j] - spectrumHistory[i - 1][j]);
                    totalChange = (change > 0) ? (totalChange + change) : totalChange;
                }

            }

            maxChange = Math.max(totalChange, maxChange);
            float changeDiff = totalChange - prevChange;
            float diffRatio = (float) NumberUtils.scale(changeDiff, 0, 2000, 0, 10);
            diffRatio = (diffRatio < 0) ? 0 : diffRatio;
            diffRatio = (diffRatio > 10) ? 10 : diffRatio;

            if (diffRatio > 0) {
                changeAverage = changeAverage + (diffRatio - changeAverage) / (changeAverageRestartCounter++);

                if (changeAverageRestartCounter >= 20) {
                    changeAverage = diffRatio;
                    changeAverageRestartCounter = 2;
                }

            }

            if ((diffRatio > changeAverage * 1.2f || diffRatio > 1.25f) && diffRatio >= 0.25f && newNoteCounter > NEW_NOTE_MIN_OFFSET /*&& !midSequence*/) {
                int skip = (diffRatio < 1) ? 1 : (int) diffRatio;
                prevSkip = skip;
                int seedX = (int) NumberUtils.scale(totalChange, 0, 2000, 0, Board.COLS);
                int seedY = (int) NumberUtils.scale(totalChange, 0, 2000, 0, Board.ROWS);
                board.generate(seedX, seedY, skip);

                Board.Cell lastAvailable = board.getLastAvailable();
                if (lastAvailable != null) {

                    float x = lastAvailable.getPosition().x + board.getPosition().x + Board.Cell.WIDTH / 2;
                    float y = lastAvailable.getPosition().y + board.getPosition().y + Board.Cell.HEIGHT / 2;

                    float distance = 0;
                    if (prevNotePosition != null && newNoteCounter < NEW_NOTE_MIN_OFFSET + 30) {
                        distance = (float) Math.sqrt(Math.pow(x - prevNotePosition.x, 2) + Math.pow(y - prevNotePosition.y, 2));
                    }

                    if (prevNoteColor != null && !ColorUtils.areColorsTheSame(prevNoteColor, background.getBaseColor()) && noteCounter > 10) {
                        noteCounter = 0;
                        switch ((seedX) % 6) {
                            case 0:
                                currentlyEmittedShape = Note.NoteShape.CIRCLE;
                                break;
                            case 1:
                                currentlyEmittedShape = Note.NoteShape.TRIANGLE;
                                break;
                            case 2:
                                currentlyEmittedShape = Note.NoteShape.SQUARE;
                                break;
                            case 4:
                                currentlyEmittedShape = Note.NoteShape.PENTAGON;
                                break;
                            case 5:
                                currentlyEmittedShape = Note.NoteShape.HEXAGON;
                                break;
                        }
                    }

                    noteCounter++;

                    float distanceLimitMax = 700;
                    float distanceLimitMin = 500;

                    if (distance < distanceLimitMax && distance > distanceLimitMin) {

                        float startX, startY;

                        if (prevNote instanceof SlideNote) {
                            startX = prevNotePosition.x;
                            startY = prevNotePosition.y;
                        } else {
                            Board.Cell previousCell = board.getCell(new Vector2(prevNotePosition.x, prevNotePosition.y));
                            Board.Cell[] unvisitedNeighbours = board.getNeighbors(board.getCol(previousCell), board.getRow(previousCell));

                            if (unvisitedNeighbours.length > 0) {
                                startX = unvisitedNeighbours[(seedX + seedY) % unvisitedNeighbours.length].getPosition().x + board.getPosition().x + Board.Cell.WIDTH / 2;
                                startY = unvisitedNeighbours[(seedX + seedY) % unvisitedNeighbours.length].getPosition().y + board.getPosition().y + Board.Cell.HEIGHT / 2;
                            } else {
                                startX = -1;
                                startY = -1;
                            }
                        }

                        Vector2 startPosition = new Vector2(startX, startY);
                        Vector2 endPosition = new Vector2(x, y);

                        float angle1To2 = (endPosition.x > startPosition.x) ? Points.getAngle(startPosition, endPosition) : Points.getAngle(endPosition, startPosition);
                        float angle2To1 = (endPosition.x <= startPosition.x) ? Points.getAngle(endPosition, startPosition) : Points.getAngle(startPosition, endPosition);

                        Vector2 parametricPoint = new Vector2((startX + x) / 2, (startY + y) / 2);
                        int maxRadius = (int) Math.abs(NumberUtils.scale(distance, distanceLimitMin, distanceLimitMax, 250, 450));
                        float r = rand.randInt(100, maxRadius);
                        float pointsAngle = (endPosition.x > startPosition.x) ? Points.getAngle(endPosition, startPosition) : Points.getAngle(startPosition, endPosition);

                        float maxAngle = (int) NumberUtils.scale(distance, distanceLimitMin, distanceLimitMax, 45, 90);
                        float angle = pointsAngle + (float) rand.randInt(15, maxAngle + 1) * NumberUtils.signum(rand.nextBoolean());

                        angle = (pointsAngle == 0) ? 0 : angle;
                        r = (pointsAngle == 0) ? 0 : r;

                        System.out.println("ANGLE: " + pointsAngle);

                        float sin = (float) Math.sin(Math.toRadians(angle));
                        float cos = (float) Math.cos(Math.toRadians(angle));
                        parametricPoint.x += r * cos;
                        parametricPoint.y += r * sin;
                        float[] bezierPathVertices = null;
                        if (r > 0) {
                            bezierPathVertices = Points.cubicBezier(startPosition, endPosition, parametricPoint);
                        }

                        boolean rebound = Points.getLength(startPosition, endPosition) < 700 && rand.randInt(0, 3) == 2;

                        final SlideNote slideNote = new SlideNote(startX, startY, x, y, NOTE_LIFESPAN, noteCounter, rebound, bezierPathVertices);
                        slideNote.setNoteColor(background.getBaseColor());

                        boolean add = getSlideNotes().size() < 2 && startX != x && startY != y && startX != -1 && startY != -1 && getFillUpNotes().size() < 1 && getSlideNotes().size() < 1;
                        if (prevNote instanceof SlideNote && ((Math.abs(prevSlideNoteAngle - angle1To2) < 20)))
                            add = false;
                        for (Note note : getSimpleNotes()) {
                            if ((note.getPosition().x == slideNote.getStartPosition().x && note.getPosition().y == slideNote.getStartPosition().y) ||
                                    (note.getPosition().x == slideNote.getEndPosition().x && note.getPosition().y == slideNote.getEndPosition().y)) {
                                add = false;
                                break;
                            }
                        }

                        if (add) {
                            notes.add(slideNote);
                            pointerCursor.goTo(slideNote.getStartPosition());
                            prevNote = slideNote;
                            prevSlideNoteAngle = angle1To2;
                            emissionInSecond++;
                        }

                    } else {

                        if (!(prevNote instanceof SlideNote && !((SlideNote) prevNote).isFinished() && prevNotePosition.x == x && prevNotePosition.y == y) && getFillUpNotes().size() < 1) {

                            boolean add = true;
                            for (SlideNote slideNote : getSlideNotes()) {
                                if ((x == slideNote.getStartPosition().x && y == slideNote.getStartPosition().y) || (x == slideNote.getEndPosition().x && y == slideNote.getEndPosition().y)) {
                                    add = false;
                                    break;
                                }
                            }

                            if (add) {
                                int total = 0;
                                if (averageHistory.size() > 3) {
                                    for (int i = averageHistory.size() - 1; i >= averageHistory.size() - 4; i--) {
                                        total += averageHistory.get(i);
                                    }
                                }

                                BaseNote newNote;

                                if (total >= 12) {
                                    int capacity = total / 3;
                                    capacity = (capacity > 6) ? 6 : capacity;
                                    capacity = (capacity < 2) ? 2 : capacity;

                                    newNote = new FillUpNote(x, y, (int) (NOTE_LIFESPAN * 1.8f), noteCounter, capacity);

                                    ((FillUpNote) newNote).setNoteColor(background.getBaseColor());
                                    averageHistory.clear();

                                } else {
                                    newNote = new Note(x, y, Note.NoteShape.CIRCLE, NOTE_LIFESPAN, noteCounter, System.currentTimeMillis());
                                    ((Note) newNote).setNoteColor(background.getBaseColor());
                                }
                                notes.add(newNote);
                                pointerCursor.goTo(newNote.getPosition());
                                prevNote = newNote;
                                emissionInSecond++;
                            }
                        }

                    }

                    prevNotePosition = new Vector2(x, y);
                    prevNoteColor = background.getBaseColor();
                }


                newNoteCounter = 0;
            }


            prevChange = totalChange;

            spectrumHistory[historyChunkIndex][historyElementIndex] = avg;

            if (historyElementIndex + 1 >= spectrumHistory[historyChunkIndex].length) {
                if (historyChunkIndex + 1 >= spectrumHistory.length) {
                    historyElementIndex = historyChunkIndex = 0;
                } else {
                    historyElementIndex = 0;
                    historyChunkIndex++;
                }
            }

            newNoteCounter++;
        }
    }

    private List<SlideNote> getSlideNotes() {
        List<SlideNote> slideNotes = new ArrayList<SlideNote>();
        for (BaseNote baseNote : notes) {
            if (baseNote instanceof SlideNote)
                slideNotes.add((SlideNote) baseNote);
        }
        return slideNotes;
    }

    private List<SlideNote> getReboundSlideNotes() {
        List<SlideNote> slideNotes = new ArrayList<SlideNote>();
        for (BaseNote baseNote : notes) {
            if (baseNote instanceof SlideNote && ((SlideNote) baseNote).isRebound())
                slideNotes.add((SlideNote) baseNote);
        }
        return slideNotes;
    }

    private List<Note> getSimpleNotes() {
        List<Note> simpleNotes = new ArrayList<Note>();
        for (BaseNote baseNote : notes) {
            if (baseNote instanceof Note)
                simpleNotes.add((Note) baseNote);
        }
        return simpleNotes;
    }

    private List<FillUpNote> getFillUpNotes() {
        List<FillUpNote> fillUpNotes = new ArrayList<FillUpNote>();
        for (BaseNote baseNote : notes) {
            if (baseNote instanceof FillUpNote)
                fillUpNotes.add((FillUpNote) baseNote);
        }
        return fillUpNotes;
    }

    public float getSecondsPassed() {
        return Player.getInstance().getProgress();
    }
}
