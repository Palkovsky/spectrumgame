package pl.dawidmacek.spectrum.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import pl.dawidmacek.spectrum.SpectrumGame;
import pl.dawidmacek.spectrum.config.Dimen;

public class GameModeScreen extends BaseScreen {

    private Viewport viewport;
    private OrthographicCamera cam;

    //Actors
    private Stage stage;
    private Table container;
    private ScrollPane songsScrollPane;

    public GameModeScreen(SpectrumGame game) {
        super(game);

        cam = new OrthographicCamera(Dimen.WIDTH, Dimen.HEIGHT);
        cam.setToOrtho(false, Dimen.WIDTH, Dimen.HEIGHT);
        viewport = new StretchViewport(Dimen.WIDTH, Dimen.HEIGHT, cam);
        cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);

        stage = new Stage(viewport, game.spriteBatch);
        createActors();
    }

    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void onBackPressed() {
        Gdx.app.exit();
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float delta) {
        stage.act(delta);
        updateActors();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

        viewport.apply();
        cam.update();
        game.spriteBatch.begin();

        game.spriteBatch.end();

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.clear();
        stage.dispose();
    }

    private void updateActors() {

    }

    private void createActors() {
        TextButton songListButton = new TextButton("LOCAL SONG", game.skin);
        songListButton.setPosition(300, Dimen.HEIGHT / 2);
        songListButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                game.setScreen(new ChooserScreen(game));
            }
        });

        TextButton microphoneButton = new TextButton("ENVIRONMENT SOUNDS", game.skin);
        microphoneButton.setPosition(Dimen.WIDTH / 2, Dimen.HEIGHT / 2);
        microphoneButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if(game.communicator.hasMicrophone()) {
                    game.setScreen(new PlayScreen(game, null));
                }else{
                    game.communicator.sendMessage("NO MICROPHONE NO PLAYING");
                }
            }
        });

        stage.addActor(songListButton);
        stage.addActor(microphoneButton);
    }
}
