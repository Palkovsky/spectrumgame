package pl.dawidmacek.spectrum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.List;

import pl.dawidmacek.spectrum.SpectrumGame;
import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.models.AudioFile;
import pl.dawidmacek.spectrum.utils.Converter;
import pl.dawidmacek.spectrum.utils.Player;


public class ChooserScreen extends BaseScreen {

    //Config
    private static final int LEFT_PANEL_PADDING = 80;
    private static final int LEFT_PANEL_WIDTH = 250;
    private static final int LEFT_PANEL_BUTTON_HEIGHT = 250;

    private static Drawable ODD_DRAWABLE = Converter.toDrawable(Color.MAROON);
    private static Drawable EVEN_DRAWABLE = Converter.toDrawable(Color.WHITE);

    private Viewport viewport;
    private OrthographicCamera cam;

    //Actors
    private Stage stage;
    private Table container;
    private ScrollPane songsScrollPane;

    public ChooserScreen(SpectrumGame game) {
        super(game);

        cam = new OrthographicCamera(Dimen.WIDTH, Dimen.HEIGHT);
        cam.setToOrtho(false, Dimen.WIDTH, Dimen.HEIGHT);
        viewport = new StretchViewport(Dimen.WIDTH, Dimen.HEIGHT, cam);
        cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);

        stage = new Stage(viewport, game.spriteBatch);
        createActors();
    }

    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void onBackPressed() {
        game.setScreen(new GameModeScreen(game));
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float delta) {
        stage.act(delta);
        updateActors();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

        viewport.apply();
        cam.update();
        game.spriteBatch.begin();

        game.spriteBatch.end();

        stage.setDebugAll(false);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        cam.position.set(cam.viewportWidth / 2, cam.viewportHeight / 2, 0);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.clear();
        stage.dispose();
    }

    private void updateActors() {

    }

    private void createActors() {

        // table that holds the scroll pane
        container = new Table();
        container.setWidth(Dimen.WIDTH);
        container.setHeight(Dimen.HEIGHT);

        Table leftPanelContainer = new Table(game.skin);
        leftPanelContainer.add(generateLeftPanelButton(new Texture(Gdx.files.internal("graphics/icons/settings.png")), "INFO", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                game.setScreen(new GameModeScreen(game));
            }
        })).padTop(40);
        leftPanelContainer.row();
        leftPanelContainer.add(generateLeftPanelButton(new Texture(Gdx.files.internal("graphics/icons/back.png")), "BACK", new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                game.setScreen(new GameModeScreen(game));
            }
        })).padTop(40);
        Color leftPanelBackground = new Color(Color.GRAY);
        leftPanelBackground.a = .6f;
        leftPanelContainer.setBackground(Converter.toDrawable(leftPanelBackground));

        final List<AudioFile> audioFiles = game.communicator.getAudioFiles();
        Table[] listItems = new Table[audioFiles.size()];
        for (int i = 0; i < audioFiles.size(); i++) {

            final Table listItem = new Table(game.skin);
            final AudioFile audioFile = audioFiles.get(i);

            Table songInfoTable = new Table(game.skin);

            int maxCharacters = 27;
            String audioTitle = audioFile.getTitle();
            audioTitle = (audioTitle.length() > maxCharacters) ? audioTitle.substring(0, maxCharacters - 3).trim() + "..." : audioTitle;

            String audioArtist = audioFile.getArtist();
            audioArtist = (audioArtist.length() > maxCharacters) ? audioArtist.substring(0, maxCharacters - 3).trim() + "..." : audioArtist;

            songInfoTable.add(new Label(audioTitle, game.skin, "default-font", "black")).left();
            songInfoTable.row();
            songInfoTable.add(new Label(audioArtist, game.skin, "default-font", "black")).left();

            listItem.add(new Image(new Texture(Gdx.files.internal("graphics/icons/music.png")))).height(120).width(140).left().padTop(40).padBottom(40).padLeft(20);
            listItem.add(songInfoTable).expand().fillY().left().padLeft(40);
            listItem.add(new Label(Converter.longToSeconds(audioFile.getDuration()), game.skin, "default-font", "black")).expand().fillY().right().padRight(20);

            listItem.setBackground((i % 2 == 0) ? EVEN_DRAWABLE : ODD_DRAWABLE);

            listItem.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    if (Player.getInstance().isValidFile(audioFile.getPath(), false)) {
                        game.setScreen(new PlayScreen(game, audioFile));
                    } else {
                        game.communicator.sendMessage("File is either corrupted or not supported.");
                    }
                }
            });

            listItems[i] = listItem;
        }


        //inner table that is used as a makeshift list.
        Table innerContainer = new Table();
        for (int i = 0; i < listItems.length; i++) {
            innerContainer.add(listItems[i]).width(Dimen.WIDTH - LEFT_PANEL_WIDTH);
            if (i < listItems.length - 1)
                innerContainer.row();
        }


        // create the scrollpane
        songsScrollPane = new ScrollPane(innerContainer);

        //add panels
        container.add(leftPanelContainer).width(LEFT_PANEL_WIDTH).height(Dimen.HEIGHT).left().expandY().top();
        container.add(songsScrollPane).left();
        container.setPosition(0, 0);

        // add container to the stage
        stage.addActor(container);

    }

    private Actor generateLeftPanelButton(Texture texture, String text, ClickListener clickListener) {
        Table innerContainer = new Table(game.skin);

        innerContainer.add(new Image(texture)).width(LEFT_PANEL_WIDTH - LEFT_PANEL_PADDING).height(LEFT_PANEL_BUTTON_HEIGHT - LEFT_PANEL_PADDING).center();
        innerContainer.row();
        innerContainer.add(new Label(text, game.skin, "default-font", "black")).expandX().center().padTop(10);
        innerContainer.addListener(clickListener);

        return innerContainer;
    }
}
