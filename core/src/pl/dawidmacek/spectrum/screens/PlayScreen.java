package pl.dawidmacek.spectrum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import aurelienribon.tweenengine.TweenManager;
import pl.dawidmacek.spectrum.SpectrumGame;
import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.handlers.PlayManager;
import pl.dawidmacek.spectrum.models.AudioFile;
import pl.dawidmacek.spectrum.utils.Player;
import pl.dawidmacek.spectrum.utils.Recorder;

public class PlayScreen extends BaseScreen {

    //Viewport
    private Viewport gameViewport, interfaceViewport, backgroundViewport;

    //Cameras
    private OrthographicCamera gameCamera, interfaceCamera, backgroundCamera;

    //Scene2D
    private Stage stage;
    private Label timestampLabel;

    //Handlers
    private PlayManager playManager;
    private TweenManager tweenManager;

    //Data
    private AudioFile audioFile;

    public PlayScreen(SpectrumGame game, AudioFile audioFile) {
        super(game);

        this.audioFile = audioFile;

        //Camera init
        gameCamera = new OrthographicCamera(Dimen.WIDTH, Dimen.HEIGHT);
        gameCamera.setToOrtho(false, Dimen.WIDTH, Dimen.HEIGHT);
        gameViewport = new StretchViewport(Dimen.WIDTH, Dimen.HEIGHT, gameCamera);
        gameCamera.position.set(gameCamera.viewportWidth / 2, gameCamera.viewportHeight / 2, 0);

        interfaceCamera = new OrthographicCamera(Dimen.WIDTH, Dimen.HEIGHT);
        interfaceCamera.setToOrtho(false, Dimen.WIDTH, Dimen.HEIGHT);
        interfaceViewport = new StretchViewport(Dimen.WIDTH, Dimen.HEIGHT, interfaceCamera);
        interfaceCamera.position.set(interfaceCamera.viewportWidth / 2, interfaceCamera.viewportHeight / 2, 0);

        backgroundCamera = new OrthographicCamera(Dimen.WIDTH, Dimen.HEIGHT);
        backgroundCamera.setToOrtho(false, Dimen.WIDTH, Dimen.HEIGHT);
        backgroundViewport = new StretchViewport(Dimen.WIDTH, Dimen.HEIGHT, backgroundCamera);
        backgroundCamera.position.set(backgroundCamera.viewportWidth / 2, backgroundCamera.viewportHeight / 2, 0);

        stage = new Stage(interfaceViewport, game.spriteBatch);
        playManager = new PlayManager(gameCamera, audioFile);
        tweenManager = new TweenManager();

        //Init methods
        createActors();
    }

    @Override
    public void onBackPressed() {
        Player.getInstance().stop();
        Player.getInstance().dispose();
        Recorder.getInstance().stop();
        game.popScreen();
        dispose();
    }

    @Override
    public void handleInput() {
        playManager.handleInput();
    }

    @Override
    public void update(float delta) {
        tweenManager.update(delta);
        playManager.update(delta);
        updateActors();
        stage.act(delta);
    }

    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(stage);
        playManager.show();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling ? GL20.GL_COVERAGE_BUFFER_BIT_NV : 0));

        backgroundViewport.apply();
        backgroundCamera.update();
        game.spriteBatch.begin();
        playManager.renderBackground(game.spriteBatch);

        gameViewport.apply();
        gameCamera.update();

        game.spriteBatch.setProjectionMatrix(gameCamera.combined);
        playManager.render(game.spriteBatch);

        game.spriteBatch.end();

        stage.draw();

        //b2dr.render(playManager.getWorld(), b2dCam.combined);

        interfaceViewport.apply();
        interfaceCamera.update();
    }

    @Override
    public void resize(int width, int height) {
        gameViewport.update(width, height);
        gameCamera.position.set(gameCamera.viewportWidth / 2, gameCamera.viewportHeight / 2, 0);
        interfaceViewport.update(width, height);
        interfaceCamera.position.set(interfaceCamera.viewportWidth / 2, interfaceCamera.viewportHeight / 2, 0);
        backgroundViewport.update(width, height);
        backgroundCamera.position.set(backgroundCamera.viewportWidth / 2, backgroundCamera.viewportHeight / 2, 0);
    }

    @Override
    public void pause() {
        System.out.println("PAUSE");
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.clear();
        stage.dispose();
        playManager.dispose();
    }

    private String format(int value) {
        return (value < 10) ? ("0" + value) : "" + value;
    }

    private void updateActors() {
        if (audioFile != null) {
            //Sets up timer
            float total = audioFile.getDuration() / 1000f;

            int totalMinutes = (int) total / 60;
            int totalSeconds = (int) total % 60;

            String stringTotal = format(totalMinutes) + ":" + format(totalSeconds);

            float current = playManager.getSecondsPassed();
            current = (current > total) ? total : current;

            int currentMinutes = (int) current / 60;
            int currentSeconds = (int) current % 60;

            String stringCurrent = format(currentMinutes) + ":" + format(currentSeconds);

            timestampLabel.setText(stringCurrent + "/" + stringTotal);
            timestampLabel.setPosition(50, 20);
        }
    }

    private void createActors() {
        if (audioFile != null) {
            timestampLabel = new Label("", game.skin, "default-font", "black");
            stage.addActor(timestampLabel);
        }
    }
}
