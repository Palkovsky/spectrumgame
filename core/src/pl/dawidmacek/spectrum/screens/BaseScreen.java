package pl.dawidmacek.spectrum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;

import pl.dawidmacek.spectrum.SpectrumGame;
import pl.dawidmacek.spectrum.utils.Bundle;

public abstract class BaseScreen implements Screen {

    private boolean backKeyActive = true;
    private boolean inputActive = true;
    private boolean backPressed = false;

    private InputProcessor lastInputProcessor;

    protected SpectrumGame game;

    public BaseScreen(SpectrumGame game) {
        this.game = game;
    }

    public BaseScreen(SpectrumGame game, Bundle bundle) {
        this.game = game;
    }

    public abstract void onBackPressed();

    public abstract void handleInput();

    public abstract void update(float delta);

    public abstract void render();

    @Override
    public void show() {
        if(Gdx.input.isKeyPressed(Input.Keys.BACK))
            backPressed = true;
    }

    @Override
    public void render(float delta) {
        update(delta);
        if (Gdx.input.getInputProcessor() != null)
            lastInputProcessor = Gdx.input.getInputProcessor();
        if (inputActive) {
            handleInput();
            Gdx.input.setInputProcessor(lastInputProcessor);
        } else
            Gdx.input.setInputProcessor(null);
        if (backKeyActive)
            isBackPressed();

        render();
    }


    private void isBackPressed() {
        if (backKeyActive && inputActive && Gdx.input.isKeyPressed(Input.Keys.BACK) && !backPressed) {
            System.out.println("ON BACK");
            onBackPressed();
            backPressed = true;
        } else if (!Gdx.input.isKeyPressed(Input.Keys.BACK))
            backPressed = false;
    }

    public void disableBackKey() {
        this.backKeyActive = false;
    }

    public void enableBackKey() {
        this.backKeyActive = true;
    }

    public void enableInput() {
        this.inputActive = true;
    }

    public void disableInput() {
        this.inputActive = false;
    }

    public boolean isInputActive() {
        return inputActive;
    }
}
