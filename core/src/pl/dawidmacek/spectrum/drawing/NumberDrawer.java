package pl.dawidmacek.spectrum.drawing;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;


public class NumberDrawer {

    private TextureRegion[] digitSheet;

    private int digitWidth;
    private int digitHeight;

    public NumberDrawer() {
        Texture dig_texture = new Texture(Gdx.files.internal("graphics/digits.png"));

        digitWidth = dig_texture.getWidth() / 5;
        digitHeight = dig_texture.getHeight() / 2;

        TextureRegion[][] regions = TextureRegion.split(dig_texture, digitWidth, digitHeight);
        digitSheet = concat(regions[0], regions[1]);
    }

    public void render(Batch spriteBatch, int number, float x, float y, float width, float height, double rotation, Color color) {

        String scoreString = String.valueOf(number);
        int length = scoreString.length();

        x += -width * (length * .5f);

        for (int i = 0; i < length; i++) {
            Character character = scoreString.charAt(i);
            int value = Character.getNumericValue(character);
            TextureRegion toDraw = digitSheet[value];

            Sprite sprite = new Sprite(toDraw);
            sprite.setOriginCenter();
            sprite.setRotation((float) rotation);
            sprite.setSize(width, height);
            sprite.setPosition(x + (i * width), y - (height / 2));
            sprite.setColor(color);
            sprite.draw(spriteBatch);
        }
    }


    private TextureRegion[] concat(TextureRegion[] a, TextureRegion[] b) {
        int aLen = a.length;
        int bLen = b.length;
        TextureRegion[] c = new TextureRegion[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }
}
