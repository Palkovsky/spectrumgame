package pl.dawidmacek.spectrum.drawing;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

public class Fonts {

    private static Fonts instance;

    //Message font
    public BitmapFont messageFont;

    private Fonts() {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("skins/fonts/LECO-1976-Stencil.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();

        parameter.size = 122;
        messageFont = generator.generateFont(parameter);

        generator.dispose();
    }

    public void dispose() {
        messageFont.dispose();
    }

    public static Fonts getInstance() {
        if (instance == null)
            instance = new Fonts();
        return instance;
    }
}
