package pl.dawidmacek.spectrum.drawing;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

import aurelienribon.tweenengine.TweenManager;
import pl.dawidmacek.spectrum.SpectrumGame;
import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.utils.ColorUtils;
import pl.dawidmacek.spectrum.utils.CustomArrayUtils;
import pl.dawidmacek.spectrum.utils.NumberUtils;
import pl.dawidmacek.spectrum.utils.Player;
import pl.dawidmacek.spectrum.utils.Vertices;

public class SpectrumBackground {

    private static final int BARS_NUMBER = 49;
    private static final float CENTER_RADIUS = 200;
    private static final Color COLOR_DEFAULT = ColorUtils.parseRGB("e91e63");

    //Data
    private float[] barsTarget = new float[BARS_NUMBER];
    private float[] barsSize = new float[BARS_NUMBER];
    private float[] barsSpeed = new float[BARS_NUMBER];
    private Color[] barsColor = new Color[BARS_NUMBER];
    private Color[] barsTargetColor = new Color[BARS_NUMBER];
    private boolean[] barsAnimatingStatus = new boolean[BARS_NUMBER];
    private Color baseColor;
    private int partitions;
    private int lastColorChangeCounter;
    private Vector2 position;
    private float avgRotation;
    private int avgRotationChangeCounter;

    //Display data
    private int currentStreak;
    private int score;

    private PolygonSprite centerSprite;
    private Sprite barSprite;
    private Sprite tintSprite;
    private TweenManager tweenManager;
    private Random rand;

    public SpectrumBackground() {

        partitions = 4;
        lastColorChangeCounter = 0;

        avgRotation = 0;
        avgRotationChangeCounter = 0;

        currentStreak = 0;
        score = 0;

        rand = new Random();
        baseColor = new Color(COLOR_DEFAULT.r, COLOR_DEFAULT.g, COLOR_DEFAULT.b, 1);
        position = new Vector2(Dimen.WIDTH / 2, Dimen.HEIGHT / 2);

        Texture texture = new Texture(Gdx.files.internal("graphics/white.png"));
        EarClippingTriangulator triangulator = new EarClippingTriangulator();

        float[] centerVertices = Vertices.circleSegment(position.x, position.y, CENTER_RADIUS - 50, 0, 361);
        TextureRegion groundTextureRegion = new TextureRegion(texture);
        PolygonRegion polygonRegion = new PolygonRegion(groundTextureRegion, centerVertices, triangulator.computeTriangles(centerVertices).toArray());

        centerSprite = new PolygonSprite(polygonRegion);
        barSprite = new Sprite(texture);
        tintSprite = new Sprite(texture);

        barSprite.setColor(COLOR_DEFAULT);
        centerSprite.setColor(barSprite.getColor());
        barSprite.setPosition(position.x, position.y);

        tintSprite.setSize(Dimen.WIDTH, Dimen.HEIGHT);
        tintSprite.setColor(ColorUtils.parseRGB("616161", 0));

        tweenManager = new TweenManager();
    }

    public void render(PolygonSpriteBatch spriteBatch) {
        tweenManager.update(Gdx.graphics.getDeltaTime());

        float[] audioSpectrum = Player.getInstance().getSpectrum(BARS_NUMBER);
        if (audioSpectrum.length > 0) {
            for (int i = 0; i < 9; i++)
                audioSpectrum[i] /= 2;
        }
        audioSpectrum = CustomArrayUtils.partitionate(audioSpectrum, partitions);

        //CustomArrayUtils.shuffle(audioSpectrum);
        float minSize = CENTER_RADIUS;
        float maxSize = minSize + 12000;
        float maxSpeed = 80f;
        float minSpeed = 10f;
        float minRotation = 0.1f;
        float maxRotation = 5f;

        float avg = 0;
        for (float value : audioSpectrum)
            avg += Math.abs(value);
        avg /= audioSpectrum.length;


        float rotation = (float) NumberUtils.scale(avg, 100, 35000, minRotation, maxRotation);
        rotation = (rotation > maxRotation) ? maxRotation : rotation;
        rotation = (rotation < minRotation) ? minRotation : rotation;

        if (!Float.isNaN(rotation)) {
            avgRotationChangeCounter++;
            avgRotation = avgRotation + (rotation - avgRotation) / avgRotationChangeCounter;
        }

        barSprite.setOrigin(barSprite.getWidth() / 2, 0);

        if (Float.isNaN(barSprite.getRotation()))
            barSprite.setRotation(0);

        if (barSprite.getRotation() > 360)
            barSprite.setRotation(barSprite.getRotation() - 360);

        if ((rotation > 1.4f || rotation > avgRotation * 2f) && lastColorChangeCounter > 12) {
            baseColor = ColorUtils.randomColor();
            lastColorChangeCounter = 0;
        }

        barSprite.setRotation(barSprite.getRotation() + rotation);

        if (audioSpectrum.length >= BARS_NUMBER) {
            for (int i = 0; i < BARS_NUMBER; i++) {

                float angle = i * ((420 / BARS_NUMBER));

                if (angle > 360)
                    break;

                if (barsColor[i] == null) {
                    barsColor[i] = barsTargetColor[i] = baseColor;
                    barsAnimatingStatus[i] = false;
                }


                barsColor[i] = baseColor;


                if (barsTarget[i] == 0 || barsTarget[i] == barsSize[i]) {
                    float size = (float) NumberUtils.scale(Math.abs(audioSpectrum[i]), 2000, 100000, minSize, maxSize);
                    size = (size > maxSize) ? maxSize : size;
                    size = (size < minSize) ? minSize : size;

                    barsTarget[i] = size;

                    float distance = Math.abs(barsTarget[i] - barsSize[i]);
                    barsSpeed[i] = (float) NumberUtils.scale(distance, 0, maxSize, minSpeed, maxSpeed);
                }

                if (barsTarget[i] < barsSize[i]) {
                    barsSize[i] -= barsSpeed[i];
                    if (barsTarget[i] > barsSize[i])
                        barsSize[i] = barsTarget[i];
                } else {
                    barsSize[i] += barsSpeed[i];
                    if (barsTarget[i] < barsSize[i])
                        barsSize[i] = barsTarget[i];
                }


                barSprite.setColor(barsColor[i]);
                barSprite.setSize(12, barsSize[i]);
                barSprite.setPosition(position.x - 5, position.y);
                barSprite.setRotation(barSprite.getRotation() + angle);
                barSprite.draw(spriteBatch);
                barSprite.setRotation(barSprite.getRotation() - angle);
            }

            centerSprite.setColor(barSprite.getColor());
            centerSprite.draw(spriteBatch);

            Color textColor = (ColorUtils.isCloserToBlack(baseColor)) ? new Color(Color.BLACK) : new Color(Color.WHITE);
            textColor.a = .6f;
            SpectrumGame.numberDrawer.render(spriteBatch, currentStreak, Dimen.WIDTH / 2, Dimen.HEIGHT / 2 + 20, 70, 140, 0, textColor);
            SpectrumGame.numberDrawer.render(spriteBatch, score, Dimen.WIDTH / 2, Dimen.HEIGHT / 2 - 100, 22, 44, 0, textColor);
        }

        if (avgRotationChangeCounter > 120) {
            avgRotationChangeCounter = 0;
            avgRotation = 0;
        }

        lastColorChangeCounter++;
        tintSprite.draw(spriteBatch);
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Color getBaseColor() {
        return baseColor;
    }

    public void setCurrentStreak(int currentStreak) {
        this.currentStreak = currentStreak;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
