package pl.dawidmacek.spectrum.drawing;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.utils.PixmapUtils;

public class PixmapBackground {

    private Pixmap basePixmap;
    private Texture pixmapTexture;

    public PixmapBackground() {
        this.basePixmap = new Pixmap((int) Dimen.WIDTH, (int) Dimen.HEIGHT, Pixmap.Format.RGB888);
        this.basePixmap.setColor(Color.WHITE);
        this.basePixmap.fillRectangle(-100, -100, (int) Dimen.WIDTH + 100, (int) Dimen.HEIGHT + 100);
        this.pixmapTexture = new Texture(basePixmap, Pixmap.Format.RGB888, false);
    }

    public void render(Batch spriteBatch) {
        spriteBatch.draw(pixmapTexture, 0, 0);
        /*
        Sprite pixmapSprite = new Sprite(pixmapTexture);
        pixmapSprite.setPosition(0, 0);
        pixmapSprite.draw(spriteBatch);
        */
    }

    public void dispose() {
        this.basePixmap.dispose();
        this.pixmapTexture.dispose();
    }

    public void addParticle(Sprite sprite, Vector2 pos, Color color) {
        int x = (int) pos.x;
        int y = (int) (Dimen.HEIGHT - pos.y);
        Color drawColor = new Color(color);
        drawColor.a = .7f;
        //basePixmap.setColor(Color.RED);


        //basePixmap.fillCircle((int) x, (int) (Dimen.HEIGHT - y), (int) Note.MAX_SIZE);
        Texture elementTexture = sprite.getTexture();
        if (!elementTexture.getTextureData().isPrepared()) {
            elementTexture.getTextureData().prepare();
        }
        Pixmap particlePixmap = elementTexture.getTextureData().consumePixmap();
        PixmapUtils.drawImageTint(basePixmap, particlePixmap, x, y, x + (int) sprite.getWidth() / 2, (int) sprite.getHeight() / 2,
                (int) sprite.getWidth(), (int) sprite.getHeight(), Color.RED);
        //basePixmap.drawPixmap();
        //basePixmap.drawPixmap(particlePixmap, x, y);


        /*
        Texture elementTexture = sprite.getTexture();
        if (!elementTexture.getTextureData().isPrepared()) {
            elementTexture.getTextureData().prepare();
        }
        Pixmap particlePixmap = elementTexture.getTextureData().consumePixmap();
        basePixmap.setColor(new Color(sprite.getColor().r, sprite.getColor().g, sprite.getColor().b, a));
        basePixmap.drawPixmap(particlePixmap, (int) sprite.getX(), (int) sprite.getY());
        */
    }

    public void buildTexture() {
        pixmapTexture.dispose();
        pixmapTexture = new Texture(basePixmap, Pixmap.Format.RGB888, false);
    }
}
