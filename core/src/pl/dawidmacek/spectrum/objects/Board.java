package pl.dawidmacek.spectrum.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import pl.dawidmacek.spectrum.config.Dimen;

public class Board {


    public static final int COLS = (int) ((Dimen.WIDTH - 200) / Cell.WIDTH);
    public static final int ROWS = (int) ((Dimen.HEIGHT - 200) / Cell.HEIGHT);
    public static final int HEIGHT = ROWS * (int) Cell.HEIGHT;
    public static final int WIDTH = COLS * (int) Cell.WIDTH;

    private static final float DEBUG_GRID_THICKNESS = 5;
    private static final boolean DRAW_DEBUG_GRID = false;
    private static final boolean DRAW_SKIPPED_CELLS = false;
    private static final boolean DRAW_AVAILABLE_CELLS = false;

    private Cell[][] cells = new Cell[COLS][ROWS];
    private List<Cell> unavailableCells;
    private Cell dummy;
    private Stack<Cell> cellStack;
    private int visitedCells;
    private Cell lastAvailable;

    //It's always left bottom vertice of board
    private Vector2 position;

    //Rendering
    private Sprite cellSprite;
    private Sprite debugBorderSprite;
    private Sprite debugMazeSprite;

    public Board(float x, float y) {
        position = new Vector2(x, y);
        dummy = new Cell(true, 0, 0);

        for (int i = 0; i < COLS; i++) {
            for (int j = 0; j < ROWS; j++) {
                cells[i][j] = new Cell(false, i * Cell.WIDTH, j * Cell.HEIGHT);
            }
        }

        cellStack = new Stack<Cell>();
        unavailableCells = new ArrayList<Cell>();
        visitedCells = 0;

        Texture whiteTexture = new Texture(Gdx.files.internal("graphics/white.png"));
        cellSprite = new Sprite(whiteTexture);
        cellSprite.setSize(Cell.WIDTH, Cell.HEIGHT);
        cellSprite.setColor(Color.CORAL);

        debugBorderSprite = new Sprite(whiteTexture);
        debugBorderSprite.setColor(Color.BLACK);

        debugMazeSprite = new Sprite(whiteTexture);
        debugMazeSprite.setColor(Color.GOLDENROD);
    }

    public Cell getCell(int col, int row) {
        if (col >= COLS || row >= ROWS || row < 0 || col < 0)
            return null;
        return cells[col][row];
    }

    public Cell getCell(Vector2 pos) {
        int col = (int) ((pos.x - position.x) / Cell.WIDTH);
        int row = (int) ((pos.y - position.y) / Cell.HEIGHT);
        return getCell(col, row);
    }

    public int getCol(Cell c) {
        return (int) ((c.getPosition().x) / Cell.WIDTH);
    }

    public int getRow(Cell c) {
        return (int) ((c.getPosition().y) / Cell.HEIGHT);
    }

    public Cell getLastAvailable() {
        return lastAvailable;
    }

    public Cell[] getNeighbors(int col, int row) {
        Cell up = (row > 0) ? getCell(col, row - 1) : dummy;
        Cell down = (row < ROWS - 1) ? getCell(col, row + 1) : dummy;
        Cell left = (col > 0) ? getCell(col - 1, row) : dummy;
        Cell right = (col < COLS - 1) ? getCell(col + 1, row) : dummy;
        return new Cell[]{up, down, left, right};
    }

    public Cell[] getUnvisitedNeighbors(int col, int row) {
        List<Cell> unvisited = new ArrayList<Cell>();
        for (Cell neighbour : getNeighbors(col, row)) {
            if (!neighbour.isVisited())
                unvisited.add(neighbour);
        }
        return unvisited.toArray(new Cell[unvisited.size()]);
    }

    public Cell[] getAvailableCells() {
        List<Cell> available = new ArrayList<Cell>();
        for (Cell[] column : cells) {
            for (Cell c : column) {
                if (c.isAvailable())
                    available.add(c);
            }
        }
        return available.toArray(new Cell[available.size()]);
    }

    public void update(float delta) {
        /*
        if (visitedCells >= 1) {
            unvisitAll();
        }
        */
    }

    public void generate(float seedX, float seedY, int skip) {
        boolean noteAdded = false;
        int skipCounter = 0;

        unvisitAll();


        for (Cell[] cols : cells) {
            for (Cell cell : cols) {
                boolean available = unavailableCells.contains(cell);
                cell.setVisited(available);
            }
        }

        while (!noteAdded) {
            if (cellStack.size() == 0) {

                if (visitedCells == 0) {
                    Cell c = cells[(int) seedX % COLS][(int) seedY % ROWS];
                    c.setAvailable(true);
                    cellStack.push(c);
                    visitedCells++;
                    lastAvailable = c;
                    noteAdded = true;
                } else {
                    unvisitAll();
                    break;
                }
            }

            Cell currentCell = cellStack.peek();
            Cell[] unvisitedNeighbours = getUnvisitedNeighbors(getCol(currentCell), getRow(currentCell));
            if (unvisitedNeighbours.length == 0) {
                cellStack.pop();
            } else {

                Cell neighbour = unvisitedNeighbours[(int) (seedX + seedY) % unvisitedNeighbours.length];

                //Find available neighbour
                int tries = 1;
                while (neighbour.isVisited()) {
                    neighbour = unvisitedNeighbours[tries];
                    tries++;
                }

                cellStack.push(neighbour);

                if (neighbour.isVisited()) {
                    unvisitAll();
                    generate(seedX, seedY, skip - 1);
                    break;
                } else {
                    neighbour.setVisited(true);
                    visitedCells++;
                    skipCounter++;
                    if (skipCounter >= skip) {
                        noteAdded = true;
                        neighbour.setAvailable(true);
                        neighbour.setVisited(true);
                        lastAvailable = neighbour;
                    }
                }
            }
        }
            /*
            Cell c = cellStack.peek();
            Cell[] unvisitedNeighbours = getUnvisitedNeighbors(getCol(c), getRow(c));
            if (unvisitedNeighbours.length == 0) {
                cellStack.pop();
            } else {
                Cell neighbour = unvisitedNeighbours[(int) (seedX + seedY) % unvisitedNeighbours.length];

                //Find available cell
                while(unavailableCells.contains(neighbour)){

                }

                neighbour.setVisited(true);
                cellStack.push(neighbour);
                visitedCells++;
                skipCounter++;
                if (skipCounter >= skip) {
                    noteAdded = true;
                    cellStack.peek().setAvailable(true);
                    lastAvailable = cellStack.peek();
                    //unvisitAll();
                } else {
                    unvisitAll();
                    generate(seedX, seedY, skip - 1, unavailableCells);
                    break;
                }
            }
        }

        if (!noteAdded)
            generate(seedX, seedY, skip - skipCounter, unavailableCells);
            */

    }

    public void unvisitAll() {
        for (Cell[] column : cells) {
            for (Cell c : column) {
                c.setVisited(false);
                c.setAvailable(false);
            }
        }
        visitedCells = 0;
    }

    public void render(PolygonSpriteBatch batch) {
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {

                Cell c = cells[i][j];

                if (DRAW_DEBUG_GRID) {
                    //Draw vertical borders
                    debugBorderSprite.setSize(DEBUG_GRID_THICKNESS, Cell.HEIGHT);
                    debugBorderSprite.setPosition(c.getPosition().x + DEBUG_GRID_THICKNESS / 2 + position.x, c.getPosition().y + position.y);
                    debugBorderSprite.draw(batch);
                    debugBorderSprite.setPosition(c.getPosition().x + WIDTH - DEBUG_GRID_THICKNESS / 2 + position.x, c.getPosition().y + position.y);
                    debugBorderSprite.draw(batch);

                    //Draw horizontal borders
                    debugBorderSprite.setSize(Cell.WIDTH, DEBUG_GRID_THICKNESS);
                    debugBorderSprite.setPosition(c.getPosition().x + position.x, c.getPosition().y + Cell.HEIGHT + DEBUG_GRID_THICKNESS / 2 + position.y);
                    debugBorderSprite.draw(batch);
                    debugBorderSprite.setPosition(c.getPosition().x + position.x, c.getPosition().y - DEBUG_GRID_THICKNESS / 2 + position.y);
                    debugBorderSprite.draw(batch);
                }

                if (DRAW_SKIPPED_CELLS) {
                    if (c.isVisited()) {
                        cellSprite.setColor(Color.GREEN);
                        cellSprite.setPosition(c.getPosition().x + position.x, c.getPosition().y + position.y);
                        cellSprite.draw(batch);
                    }
                }


                if (DRAW_AVAILABLE_CELLS) {
                    if (c.isAvailable()) {
                        cellSprite.setColor(Color.BLUE);
                        cellSprite.setPosition(c.getPosition().x + position.x, c.getPosition().y + position.y);
                        cellSprite.draw(batch);
                    }
                }

            }
        }
    }

    public void setUnavailableCells(List<Cell> unavailableCells) {
        this.unavailableCells = unavailableCells;
    }

    public Vector2 getPosition() {
        return position;
    }

    public class Cell {

        public static final float WIDTH = 200;
        public static final float HEIGHT = 200;

        private boolean visited;
        private boolean available;

        Vector2 position;

        public Cell(boolean visited, float x, float y) {
            this.visited = visited;
            this.position = new Vector2(x, y);
            this.available = false;
        }

        public boolean isAvailable() {
            return available;
        }

        public void setAvailable(boolean available) {
            this.available = available;
        }

        public Vector2 getPosition() {
            return position;
        }

        public boolean isVisited() {
            return visited;
        }

        public void setVisited(boolean visited) {
            this.visited = visited;
        }
    }
}
