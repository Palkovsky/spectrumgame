package pl.dawidmacek.spectrum.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Linear;
import pl.dawidmacek.spectrum.ui.tweens.ProgressBarTween;

public class ProgressBar {

    //Rendering data
    private float x, y;
    private float width, height;
    private Color backgroundColor, progressColor;

    //Rendering
    private Sprite backgroundSprite, progressSprite;

    //Data
    private Float progress;
    private boolean animating;

    //Handlers
    private TweenManager tweenManager;

    public ProgressBar(float x, float y, float width, float height, Color backgroundColor, Color progressColor) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.backgroundColor = backgroundColor;
        this.progressColor = progressColor;
        this.progress = 0f;
        this.tweenManager = new TweenManager();
        this.animating = false;

        Texture baseTexture = new Texture(Gdx.files.internal("graphics/white.png"));
        backgroundSprite = new Sprite(baseTexture);
        progressSprite = new Sprite(baseTexture);

        backgroundSprite.setSize(width, height);
        backgroundSprite.setColor(backgroundColor);
        backgroundSprite.setPosition(x, y);

        progressSprite.setSize(0, height);
        progressSprite.setColor(progressColor);
        progressSprite.setPosition(x, y);
    }

    public void render(Batch batch) {
        tweenManager.update(Gdx.graphics.getDeltaTime());
        progressSprite.setSize(width * (progress / 100f), height);

        backgroundSprite.draw(batch);
        progressSprite.draw(batch);
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress){
        this.progress = progress;
    }

    public void animateProgress(float progress) {
        if(!animating) {
            Tween.to(this, ProgressBarTween.PROGRESS, .1f)
                    .target(progress)
                    .ease(Linear.INOUT)
                    .setCallback(new TweenCallback() {
                        @Override
                        public void onEvent(int type, BaseTween<?> source) {
                            animating = false;
                        }
                    })
                    .start(tweenManager);
            animating = true;
        }
    }
}
