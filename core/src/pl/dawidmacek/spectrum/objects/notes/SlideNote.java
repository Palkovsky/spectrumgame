package pl.dawidmacek.spectrum.objects.notes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;
import pl.dawidmacek.spectrum.SpectrumGame;
import pl.dawidmacek.spectrum.ui.tweens.SlideNoteTween;
import pl.dawidmacek.spectrum.utils.ColorUtils;
import pl.dawidmacek.spectrum.utils.Points;
import pl.dawidmacek.spectrum.utils.Vertices;

public class SlideNote extends pl.dawidmacek.spectrum.objects.notes.BaseNote {

    //Config
    private static final float MAX_RADIUS = 100;
    private static final float MAX_OUTER_RING_SIZE = 10;
    private static final float MIN_GROW_SPEED = 5;

    //Data
    private Vector2 startPosition;
    private Vector2 endPosition;
    private Vector2 slideStartPosition, slideDestinationPosition;
    private Vector2 touchPosition;
    private int lifespan;
    private int lifetime;
    private int noteNum;
    private boolean sliding;
    private boolean slidingFinished;
    private boolean finishAnimation;
    private boolean finished;
    private boolean initialTouchIndicated;
    private boolean touchReleaseIndicated;
    private boolean rebound;
    private boolean reboundReached;
    private float radius;
    private float startNoteRadiusOffset, endNoteRadiusOffset;
    private float outerRingSize;
    private float[] pathVertices;
    private float[] drawPathVertices;
    private float[] noteStartVertices, noteEndVertices;
    private Color noteColor, ringColor;

    //Rendering
    private PolygonSprite pathSprite;
    private PolygonSprite noteSprite;
    private PolygonSprite touchIndicatorSprite;
    private EarClippingTriangulator triangulator;
    private TextureRegion whiteTextureRegion;

    //Utils
    private TweenManager tweenManager;

    public SlideNote(float startX, float startY, float endX, float endY, int lifespan, int noteNum, boolean rebound, float[] pathVertices) {
        this.startPosition = new Vector2(startX, startY);
        this.endPosition = new Vector2(endX, endY);
        this.lifespan = lifespan;
        this.noteNum = noteNum;
        this.rebound = rebound;
        this.pathVertices = pathVertices;
        this.drawPathVertices = new float[]{};
        this.finished = false;
        this.finishAnimation = false;
        this.slidingFinished = false;
        this.initialTouchIndicated = false;
        this.touchReleaseIndicated = false;
        this.reboundReached = false;
        this.radius = this.outerRingSize = 0;
        this.startNoteRadiusOffset = this.endNoteRadiusOffset = 0;
        this.lifetime = 0;

        this.noteColor = ColorUtils.parseRGB("d50000");
        this.ringColor = ColorUtils.parseRGB("ff3d00", .9f);

        this.triangulator = new EarClippingTriangulator();
        this.whiteTextureRegion = new TextureRegion(new Texture(Gdx.files.internal("graphics/white.png")));

        this.tweenManager = new TweenManager();
    }

    public SlideNote(pl.dawidmacek.spectrum.objects.notes.Note startNote, pl.dawidmacek.spectrum.objects.notes.Note endNote, int lifespan, int noteNum, boolean rebound, float[] pathVertices) {
        this(startNote.getPosition().x, startNote.getPosition().y, endNote.getPosition().x, endNote.getPosition().y, lifespan, noteNum, rebound, pathVertices);
    }

    public void update(float delta) {
        tweenManager.update(delta);
        lifetime++;

        float radiusGrow = MIN_GROW_SPEED;
        float outerRadiusGrow = 1;

        if (!finished && !sliding) {

            if (finishAnimation) {
                radius -= radiusGrow * 3;
                outerRingSize -= outerRadiusGrow * 2;
                finished = (radius <= 0);
            } else {
                radius += radiusGrow;
                outerRingSize += outerRadiusGrow;

                radius = (radius > MAX_RADIUS) ? MAX_RADIUS : radius;
                outerRingSize = (outerRingSize > MAX_OUTER_RING_SIZE) ? MAX_OUTER_RING_SIZE : outerRingSize;
            }

            if (!touchReleaseIndicated) {
                float indicationDuration = .2f;

                Timeline.createParallel().push(
                        Tween.to(this, SlideNoteTween.END_NOTE_RADIUS_OFFSET, indicationDuration / 2f).target(0).build()
                ).push(
                        Tween.to(this, SlideNoteTween.START_NOTE_RADIUS_OFFSET, indicationDuration / 2f).target(0).build()
                ).start(tweenManager);

                initialTouchIndicated = false;
            }
        }

        if (sliding && !initialTouchIndicated) {

            touchReleaseIndicated = false;
            float indicationDuration = .2f;
            float indicationOffsetSize = 30;

            Timeline.createSequence()
                    .push(
                            Timeline.createParallel().push(
                                    Tween.to(this, SlideNoteTween.END_NOTE_RADIUS_OFFSET, indicationDuration / 2f).target(indicationOffsetSize).build()
                            ).push(
                                    Tween.to(this, SlideNoteTween.START_NOTE_RADIUS_OFFSET, indicationDuration / 2f).target(indicationOffsetSize).build()
                            )
                    ).start(tweenManager);

            initialTouchIndicated = true;
        }
    }

    @Override
    public Vector2 getPosition() {
        return null;
    }

    public void render(PolygonSpriteBatch batch) {

        //Path drawing
        float offset = (endNoteRadiusOffset > startNoteRadiusOffset) ? endNoteRadiusOffset : startNoteRadiusOffset;
        float[] pathVertices = calculatePathVertices(new Vector2[]{startPosition, endPosition}, radius * 2 + offset);
        drawPathVertices = pathVertices;
        PolygonRegion pathPolygonRegion = new PolygonRegion(whiteTextureRegion, pathVertices, triangulator.computeTriangles(pathVertices).toArray());
        if (pathSprite == null)
            pathSprite = new PolygonSprite(pathPolygonRegion);
        else
            pathSprite.setRegion(pathPolygonRegion);
        noteColor.a = (isSliding()) ? 1f : .75f;
        pathSprite.setColor(noteColor);
        noteColor.a = 1f;
        pathSprite.draw(batch);

        //Touch highlight render
        if (sliding) {

            Color highlightColor = (!ColorUtils.isCloserToBlack(noteColor)) ? Color.WHITE : Color.BLACK;
            highlightColor.a = .9f;

            Vector2 touchCenter = calculateHighlightPosition(new Vector2[]{startPosition, endPosition}, touchPosition);
            float[] touchIndicatorVertices = Vertices.circleSegment(touchCenter.x, touchCenter.y, radius, 0, 361);
            PolygonRegion touchIndicatorPolygonRegion = new PolygonRegion(whiteTextureRegion, touchIndicatorVertices, triangulator.computeTriangles(touchIndicatorVertices).toArray());
            if (touchIndicatorSprite == null)
                touchIndicatorSprite = new PolygonSprite(touchIndicatorPolygonRegion);
            else
                touchIndicatorSprite.setRegion(touchIndicatorPolygonRegion);
            touchIndicatorSprite.setColor(highlightColor);
            touchIndicatorSprite.draw(batch);

        }

        //Start Note drawing
        float[] outerNoteVertices = Vertices.circleSegment(startPosition.x, startPosition.y, radius + startNoteRadiusOffset + outerRingSize * 1.4f, 0, 361);
        float[] noteVertices = Vertices.circleSegment(startPosition.x, startPosition.y, radius + startNoteRadiusOffset, 0, 361);
        this.noteStartVertices = noteVertices;

        PolygonRegion outerNotePolygonRegion = new PolygonRegion(whiteTextureRegion, outerNoteVertices, triangulator.computeTriangles(outerNoteVertices).toArray());
        PolygonRegion notePolygonRegion = new PolygonRegion(whiteTextureRegion, noteVertices, triangulator.computeTriangles(noteVertices).toArray());

        if (noteSprite == null)
            noteSprite = new PolygonSprite(outerNotePolygonRegion);
        else
            noteSprite.setRegion(outerNotePolygonRegion);
        noteSprite.setColor(ringColor);
        noteSprite.draw(batch);

        noteSprite.setRegion(notePolygonRegion);
        noteSprite.setColor(noteColor);
        noteSprite.draw(batch);


        //End note drawing
        outerNoteVertices = Vertices.circleSegment(endPosition.x, endPosition.y, radius + endNoteRadiusOffset + outerRingSize * 1.4f, 0, 361);
        noteVertices = Vertices.circleSegment(endPosition.x, endPosition.y, radius + endNoteRadiusOffset, 0, 361);
        this.noteEndVertices = noteVertices;

        outerNotePolygonRegion = new PolygonRegion(whiteTextureRegion, outerNoteVertices, triangulator.computeTriangles(outerNoteVertices).toArray());
        notePolygonRegion = new PolygonRegion(whiteTextureRegion, noteVertices, triangulator.computeTriangles(noteVertices).toArray());

        noteSprite.setRegion(outerNotePolygonRegion);
        noteSprite.setColor(ringColor);
        noteSprite.draw(batch);

        noteSprite.setRegion(notePolygonRegion);
        if (rebound && !reboundReached) {
            noteSprite.setColor(ColorUtils.parseRGB("757575"));
        } else {
            noteSprite.setColor(noteColor);
        }
        noteSprite.draw(batch);

        if (!ColorUtils.isCloserToBlack(noteColor)) {
            SpectrumGame.numberDrawer.render(batch, noteNum, startPosition.x, startPosition.y, radius / 2, radius, 0, Color.WHITE);
            if (!rebound)
                SpectrumGame.numberDrawer.render(batch, noteNum, endPosition.x, endPosition.y, radius / 2, radius, 0, Color.WHITE);
        } else {
            SpectrumGame.numberDrawer.render(batch, noteNum, startPosition.x, startPosition.y, radius / 2, radius, 0, Color.BLACK);
            if (!rebound)
                SpectrumGame.numberDrawer.render(batch, noteNum, endPosition.x, endPosition.y, radius / 2, radius, 0, Color.BLACK);
        }

    }

    public boolean hasEnded() {
        return (lifetime >= lifespan || slidingFinished);
    }

    public void setNoteColor(Color noteColor) {
        this.noteColor = noteColor;
    }

    private Vector2 calculateHighlightPosition(Vector2[] points, Vector2 touchPosition) {


        if (pathVertices != null) {
            points = new Vector2[pathVertices.length / 2];
            for (int i = 0; i < pathVertices.length; i += 2) {
                points[i / 2] = new Vector2(pathVertices[i], pathVertices[i + 1]);
            }
        }

        //Perform calculations to make path vertices more accurate
        List<Vector2> accuratePoints = new ArrayList<Vector2>();
        for (int i = 1; i < points.length; i++) {
            Vector2 startPoint = points[i - 1];
            Vector2 endPoint = points[i];


            float stepSize = 1f;
            float xDistance = endPoint.x - startPoint.x;
            float yDistance = endPoint.y - startPoint.y;
            float diagonalDistance = (float) Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
            int steps = (int) (diagonalDistance / stepSize);
            float stepX = xDistance / steps;
            float stepY = yDistance / steps;

            for (int j = 0; j < steps; j++) {
                accuratePoints.add(new Vector2(startPoint.x + stepX * j, startPoint.y + stepY * j));
            }
        }

        points = accuratePoints.toArray(new Vector2[accuratePoints.size()]);

        int closestPointIndex = 0;
        float smallestDistance = 0;
        for (int i = 0; i < points.length; i++) {
            float distance = Points.getLength(points[i], touchPosition);
            if (distance < smallestDistance || i == 0) {
                smallestDistance = distance;
                closestPointIndex = i;
            }
        }

        Vector2 touchCenter = new Vector2();
        if (points.length >= 2) {
            Vector2 startPoint = (closestPointIndex == 0) ? points[0] : points[closestPointIndex - 1];
            Vector2 endPoint = (closestPointIndex == 0) ? points[1] : points[closestPointIndex];

            float xDistance = endPoint.x - startPoint.x;
            float yDistance = endPoint.y - startPoint.y;

            touchCenter = new Vector2(startPoint.x + (xDistance / 2), startPoint.y + (yDistance / 2));
        }


        return touchCenter;
    }


    private float[] calculatePathVertices(Vector2[] points, float size) {


        if (pathVertices != null) {
            points = new Vector2[pathVertices.length / 2];
            for (int i = 0; i < pathVertices.length; i += 2) {
                points[i / 2] = new Vector2(pathVertices[i], pathVertices[i + 1]);
            }
        }

        float[] vertices = new float[points.length * 4];
        int j = 0;

        float halvedSize = size / 2;

        for (int i = 1; i < points.length; i++) {
            Vector2 startPoint = points[i - 1];
            Vector2 endPoint = points[i];

            float angle = Points.getAngle(startPoint, endPoint)+90;

            float sin = (float) Math.sin(Math.toRadians(angle));
            float cos = (float) Math.cos(Math.toRadians(angle));

            if (i == 1) {
                vertices[j] = startPoint.x + halvedSize * cos;
                vertices[j + 1] = startPoint.y + halvedSize * sin;
                vertices[j + 2] = endPoint.x + halvedSize * cos;
                vertices[j + 3] = endPoint.y + halvedSize * sin;
                j += 4;
            } else {
                vertices[j] = endPoint.x + halvedSize * cos;
                vertices[j + 1] = endPoint.y + halvedSize * sin;
                j += 2;
            }

        }


        for (int i = points.length - 1; i > 0; i--) {
            Vector2 startPoint = points[i];
            Vector2 endPoint = points[i - 1];

            float angle = Points.getAngle(startPoint, endPoint) - 90;

            float sin = (float) Math.sin(Math.toRadians(angle));
            float cos = (float) Math.cos(Math.toRadians(angle));

            if (i == points.length - 1) {
                vertices[j] = startPoint.x - halvedSize * cos;
                vertices[j + 1] = startPoint.y - halvedSize * sin;
                vertices[j + 2] = endPoint.x - halvedSize * cos;
                vertices[j + 3] = endPoint.y - halvedSize * sin;
                j += 4;
            } else {
                vertices[j] = endPoint.x - halvedSize * cos;
                vertices[j + 1] = endPoint.y - halvedSize * sin;
                j += 2;
            }
        }

        return vertices;
    }


    public void startSliding(Vector2 point) {
        sliding = true;
        initialTouchIndicated = false;

        if (doesEndNoteContainsPoint(point)) {
            slideDestinationPosition = startPosition;
            slideStartPosition = endPosition;
        } else {
            slideDestinationPosition = endPosition;
            slideStartPosition = startPosition;
        }
    }

    public void setSlidingSuccess(boolean success) {
        if (rebound && !reboundReached && success) {
            Vector2 tmpStartPosition = slideStartPosition;
            slideStartPosition = slideDestinationPosition;
            slideDestinationPosition = tmpStartPosition;
            reboundReached = true;
        } else {
            this.slidingFinished = true;
            this.sliding = false;
            finish();
        }
    }

    public void interruptSliding() {
        this.sliding = false;
        this.finishAnimation = false;
        this.initialTouchIndicated = false;
        this.finished = false;
        this.reboundReached = false;
        this.slideDestinationPosition = this.slideStartPosition = null;
    }

    public Vector2[] linePoints() {
        List<Vector2> points = new ArrayList<Vector2>(Arrays.asList(Points.verticesToVectors(drawPathVertices)));
        if (pathVertices != null)
            points.addAll(new ArrayList<Vector2>(Arrays.asList(Points.verticesToVectors(pathVertices))));
        /*
        if(noteStartVertices != null)
            points.addAll(new ArrayList<Vector2>(Arrays.asList(Points.verticesToVectors(noteStartVertices))));
        if(noteEndVertices != null)
            points.addAll(new ArrayList<Vector2>(Arrays.asList(Points.verticesToVectors(noteEndVertices))));
        */
        return points.toArray(new Vector2[points.size()]);
    }

    public void finish() {
        finishAnimation = true;
    }

    public boolean isSliding() {
        return sliding;
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isFinishing() {
        return finishAnimation;
    }


    public boolean doesEndNoteContainsPoint(Vector2 point) {
        return (
                point.x >= endPosition.x - MAX_RADIUS
                        && point.x <= endPosition.x + MAX_RADIUS
                        && point.y >= endPosition.y - MAX_RADIUS
                        && point.y <= endPosition.y + MAX_RADIUS
        );
    }

    public boolean doesStartNoteContainsPoint(Vector2 point) {
        return (
                point.x >= startPosition.x - MAX_RADIUS
                        && point.x <= startPosition.x + MAX_RADIUS
                        && point.y >= startPosition.y - MAX_RADIUS
                        && point.y <= startPosition.y + MAX_RADIUS
        );
    }

    public boolean doesDestinationNoteContainsPoint(Vector2 point) {
        return (
                point.x >= slideDestinationPosition.x - MAX_RADIUS
                        && point.x <= slideDestinationPosition.x + MAX_RADIUS
                        && point.y >= slideDestinationPosition.y - MAX_RADIUS
                        && point.y <= slideDestinationPosition.y + MAX_RADIUS
        );
    }

    public boolean doesPathContainsPoint(Vector2 point) {
        float[] pathVertices = calculatePathVertices(new Vector2[]{startPosition, endPosition}, MAX_RADIUS * 2);
        Polygon pathPolygon = new Polygon(pathVertices);
        return pathPolygon.contains(point);
    }

    public Vector2 getStartPosition() {
        return startPosition;
    }

    public Vector2 getEndPosition() {
        return endPosition;
    }

    public float getStartNoteRadiusOffset() {
        return startNoteRadiusOffset;
    }

    public void setStartNoteRadiusOffset(float startNoteRadiusOffset) {
        this.startNoteRadiusOffset = startNoteRadiusOffset;
    }

    public boolean isRebound() {
        return rebound;
    }

    public boolean isReboundReached() {
        return reboundReached;
    }

    public float getEndNoteRadiusOffset() {
        return endNoteRadiusOffset;
    }

    public void setEndNoteRadiusOffset(float endNoteRadiusOffset) {
        this.endNoteRadiusOffset = endNoteRadiusOffset;
    }

    public void setTouchPosition(Vector2 touchPosition) {
        this.touchPosition = touchPosition;
    }

    public Color getRingColor() {
        return ringColor;
    }

    public Color getNoteColor() {
        return noteColor;
    }
}
