package pl.dawidmacek.spectrum.objects.notes;


import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.math.Vector2;

public abstract class BaseNote {
    public abstract void render(PolygonSpriteBatch batch);

    public abstract void update(float delta);

    public abstract Vector2 getPosition();

    public abstract void finish();
    public abstract boolean hasEnded();
    public abstract boolean isFinishing();
}
