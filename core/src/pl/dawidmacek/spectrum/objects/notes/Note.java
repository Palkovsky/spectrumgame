package pl.dawidmacek.spectrum.objects.notes;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;

import pl.dawidmacek.spectrum.SpectrumGame;
import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.utils.ColorUtils;
import pl.dawidmacek.spectrum.utils.NumberUtils;
import pl.dawidmacek.spectrum.utils.Player;
import pl.dawidmacek.spectrum.utils.Points;
import pl.dawidmacek.spectrum.utils.Vertices;

public class Note extends pl.dawidmacek.spectrum.objects.notes.BaseNote {

    public enum NoteShape {
        CIRCLE, TRIANGLE, SQUARE, PENTAGON, HEXAGON
    }

    public static final float MAX_SIZE = 100;
    private static final float MAX_OUTER_SHAPE_SIZE = 10;
    private static final float MIN_GROW_SPEED = 5;

    //Data
    private Vector2 position;
    private Color noteColor, ringColor;
    private int lifespan;
    private int lifetime;
    private int noteNum;
    private boolean tapped;
    private boolean finishAnimation;
    private boolean finished;
    private float size;
    private float outerShapeSize;
    private float growSpeed;
    private float[] drawNoteVertices;
    private float rotation = 0;
    private long emittedAt;

    //Rendering
    private NoteShape shape;
    private EarClippingTriangulator triangulator;
    private TextureRegion noteTextureRegion;
    private PolygonSprite noteSprite;


    public Note(float x, float y, NoteShape shape, int lifespan, int noteNum, long emmitedAt) {
        this.position = new Vector2(x, y);
        this.shape = shape;
        this.lifespan = lifespan;
        this.noteNum = noteNum;
        this.emittedAt = emmitedAt;
        this.finished = false;
        this.finishAnimation = false;
        this.lifetime = 0;
        this.size = this.outerShapeSize = 0;
        this.growSpeed = MIN_GROW_SPEED;
        this.drawNoteVertices = new float[]{};

        this.noteColor = ColorUtils.parseRGB("d50000", .8f);
        this.ringColor = ColorUtils.parseRGB("212121", .9f);

        this.triangulator = new EarClippingTriangulator();
        this.noteTextureRegion = new TextureRegion(new Texture(Gdx.files.internal("graphics/white.png")));
    }

    public void update(float delta) {
        long sinceEmitt = System.currentTimeMillis() - emittedAt;
        if (sinceEmitt > Player.SPECTRUM_PLAYER_OFFSET * .7f * 1000) {
            lifetime++;
        }

        float sizeGrow = MIN_GROW_SPEED;
        float outerSizeGrow = 1;

        if (!finished) {
            if (finishAnimation) {
                size -= sizeGrow * 3;
                outerShapeSize -= outerSizeGrow * 2;
                finished = (size <= 0);
            } else {
                size += sizeGrow;
                outerShapeSize += outerSizeGrow;

                size = (size > MAX_SIZE) ? MAX_SIZE : size;
                outerShapeSize = (outerShapeSize > MAX_OUTER_SHAPE_SIZE) ? MAX_OUTER_SHAPE_SIZE : outerShapeSize;
            }
        }

    }

    public void render(PolygonSpriteBatch batch) {

        float[] outerNoteVertices = new float[0];
        float[] noteVertices = new float[0];

        float drawOuterShapeSize = outerShapeSize;
        rotation = Points.getAngle(position, new Vector2(Dimen.WIDTH / 2, Dimen.HEIGHT / 2)) - 90;

        switch (shape) {
            case CIRCLE:
                drawOuterShapeSize *= 1.7f;
                outerNoteVertices = Vertices.circleSegment(position.x, position.y, size + drawOuterShapeSize, 0, 361);
                noteVertices = Vertices.circleSegment(position.x, position.y, size, 0, 361);
                break;
            case TRIANGLE:
                drawOuterShapeSize *= 3;
                outerNoteVertices = Vertices.triangle(position.x, position.y, 200 + drawOuterShapeSize, size + drawOuterShapeSize, rotation);
                noteVertices = Vertices.triangle(position.x, position.y, 200 - drawOuterShapeSize, size, rotation);
                //outerNoteVertices = Vertices.regularPolygon(position.x, position.y, size + drawOuterShapeSize, 3);
                //noteVertices = Vertices.regularPolygon(position.x, position.y, size, 6);
                break;
            case SQUARE:
                drawOuterShapeSize *= 2.3f;
                outerNoteVertices = Vertices.rectangle(position.x, position.y, (size + drawOuterShapeSize) * 1.75f, (size + drawOuterShapeSize) * 1.75f, rotation);
                noteVertices = Vertices.rectangle(position.x, position.y, size * 1.75f, size * 1.75f, rotation);
                break;
            case PENTAGON:
                drawOuterShapeSize *= 2;
                outerNoteVertices = Vertices.regularPolygon(position.x, position.y, (size + drawOuterShapeSize) * 1.1f, 5, rotation);
                noteVertices = Vertices.regularPolygon(position.x, position.y, size * 1.1f, 5, rotation);
                break;
            case HEXAGON:
                drawOuterShapeSize *= 2;
                outerNoteVertices = Vertices.regularPolygon(position.x, position.y, (size + drawOuterShapeSize) * 1.1f, 6, rotation);
                noteVertices = Vertices.regularPolygon(position.x, position.y, size * 1.1f, 6, rotation);
                break;
        }

        drawNoteVertices = noteVertices;

        PolygonRegion outerNotePolygonRegion = new PolygonRegion(noteTextureRegion, outerNoteVertices, triangulator.computeTriangles(outerNoteVertices).toArray());
        PolygonRegion notePolygonRegion = new PolygonRegion(noteTextureRegion, noteVertices, triangulator.computeTriangles(noteVertices).toArray());

        if (noteSprite == null)
            noteSprite = new PolygonSprite(outerNotePolygonRegion);
        else
            noteSprite.setRegion(outerNotePolygonRegion);
        noteSprite.setColor(ringColor);
        noteSprite.draw(batch);

        noteSprite.setRegion(notePolygonRegion);
        noteSprite.setColor(noteColor);
        noteSprite.draw(batch);


        float maxRhythmRadiusSize = size + drawOuterShapeSize + 150;
        float minRhythmRadiusSize = size + drawOuterShapeSize;
        long sinceEmitt = System.currentTimeMillis() - emittedAt;
        float rhythmRadius = (float) NumberUtils.scale(sinceEmitt, Player.SPECTRUM_PLAYER_OFFSET * 1000, 0, minRhythmRadiusSize, maxRhythmRadiusSize);
        if (rhythmRadius >= minRhythmRadiusSize && rhythmRadius <= maxRhythmRadiusSize) {
            float[] rhythmRingVertices = Vertices.ring(position.x, position.y, rhythmRadius, 18, 0, 361, 0.1f);
            PolygonRegion rhythmRingPolygonRegion = new PolygonRegion(noteTextureRegion, rhythmRingVertices, triangulator.computeTriangles(rhythmRingVertices).toArray());
            noteSprite.setRegion(rhythmRingPolygonRegion);
            noteSprite.setColor(Color.GRAY);
            noteSprite.draw(batch);
        }


        Color textColor = (ColorUtils.isCloserToBlack(noteColor)) ? Color.BLACK : Color.WHITE;
        switch (shape) {
            case HEXAGON:
            case PENTAGON:
            case SQUARE:
            case CIRCLE:
                SpectrumGame.numberDrawer.render(batch, noteNum, position.x, position.y, size / 2, size, 0, textColor);
                break;
            case TRIANGLE:
                SpectrumGame.numberDrawer.render(batch, noteNum, position.x, position.y, size / 2, size, 0, textColor);
                break;
        }

    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void setGrowSpeed(float growSpeed) {
        this.growSpeed = growSpeed;
    }

    public boolean hasEnded() {
        return (lifetime >= lifespan);
    }

    public void tap() {
        tapped = true;
        finishAnimation = true;
    }

    public void finish() {
        finishAnimation = true;
    }

    public boolean isTapped() {
        return tapped && finished;
    }

    public boolean isFinishing() {
        return finishAnimation;
    }

    public boolean containsPoint(Vector2 point) {
        return (
                point.x >= position.x - MAX_SIZE
                        && point.x <= position.x + MAX_SIZE
                        && point.y >= position.y - MAX_SIZE
                        && point.y <= position.y + MAX_SIZE
        );
    }

    public void setNoteColor(Color noteColor) {
        this.noteColor = new Color(noteColor.r, noteColor.g, noteColor.b, noteColor.a);
    }

    public Vector2[] linePoints() {
        //return Points.verticesToVectors(drawNoteVertices);
        return new Vector2[]{};
    }

    public boolean isFinished() {
        return finished;
    }

    public Color getNoteColor() {
        return noteColor;
    }

    public Color getRingColor() {
        return ringColor;
    }

    public NoteShape getShape() {
        return shape;
    }
}
