package pl.dawidmacek.spectrum.objects.notes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import pl.dawidmacek.spectrum.SpectrumGame;
import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.objects.Message;
import pl.dawidmacek.spectrum.ui.tweens.FillUpNoteTween;
import pl.dawidmacek.spectrum.utils.ColorUtils;
import pl.dawidmacek.spectrum.utils.Vertices;


public class FillUpNote extends BaseNote {

    private static final float MAX_SIZE = 200;
    private static final float FRAME_SIZE = 60;

    //Data
    private Vector2 position;
    private Color noteColor, ringColor;
    private float size;
    private float outerSize;
    private int lifespan;
    private int lifetime;
    private int noteNum;
    private int capacity;
    private float currentFill;
    private float sizeOffset;
    private boolean animating;

    private boolean finished;
    private boolean finishAnimation;

    //Rendering
    private EarClippingTriangulator triangulator;
    private TextureRegion noteTextureRegion;
    private PolygonSprite noteSprite;
    private Message capacityMessage;

    //Handlers
    private TweenManager tweenManager;

    public FillUpNote(float x, float y, int lifespan, int noteNum, int capacity) {
        this.position = new Vector2(x, y);
        this.lifespan = lifespan;
        this.noteNum = noteNum;
        this.capacity = capacity;

        this.size = this.outerSize = 0;
        this.currentFill = 0;
        this.sizeOffset = 0;
        this.animating = false;
        this.finished = false;
        this.finishAnimation = false;
        this.noteColor = ColorUtils.parseRGB("d50000", .8f);
        this.ringColor = ColorUtils.parseRGB("1b5e20", 1f);

        this.triangulator = new EarClippingTriangulator();
        this.noteTextureRegion = new TextureRegion(new Texture(Gdx.files.internal("graphics/white.png")));

        this.tweenManager = new TweenManager();


        float messageX = (x > Dimen.WIDTH / 2) ? x - MAX_SIZE : x + MAX_SIZE;
        this.capacityMessage = new Message(messageX, y, Color.BLUE, "", 20);
    }

    @Override
    public void render(PolygonSpriteBatch batch) {

        float[] outerNoteVertices = Vertices.rectangle(position.x, position.y, size + outerSize + sizeOffset, size + outerSize + sizeOffset, 0);
        float[] noteVertices = Vertices.rectangle(position.x, position.y, size + sizeOffset, size + sizeOffset, 0);
        float fillHeight = (size + sizeOffset) * (currentFill / capacity);
        float[] fillVertices = Vertices.rectangle(position.x, position.y - (size + sizeOffset) / 2 + fillHeight / 2f, (size + sizeOffset), fillHeight, 0);

        PolygonRegion outerNotePolygonRegion = new PolygonRegion(noteTextureRegion, outerNoteVertices, triangulator.computeTriangles(outerNoteVertices).toArray());
        PolygonRegion notePolygonRegion = new PolygonRegion(noteTextureRegion, noteVertices, triangulator.computeTriangles(noteVertices).toArray());
        PolygonRegion fillPolygonRegion = new PolygonRegion(noteTextureRegion, fillVertices, triangulator.computeTriangles(noteVertices).toArray());

        if (noteSprite == null)
            noteSprite = new PolygonSprite(outerNotePolygonRegion);
        else
            noteSprite.setRegion(outerNotePolygonRegion);
        noteSprite.setColor(ringColor);
        noteSprite.draw(batch);

        noteSprite.setRegion(notePolygonRegion);
        noteSprite.setColor(Color.GRAY);
        noteSprite.draw(batch);

        noteSprite.setRegion(fillPolygonRegion);
        noteSprite.setColor(new Color(noteColor.r, noteColor.g, noteColor.b, 1f));
        noteSprite.draw(batch);

        Color textColor = (ColorUtils.isCloserToBlack(noteColor)) ? Color.BLACK : Color.WHITE;
        SpectrumGame.numberDrawer.render(batch, noteNum, position.x, position.y, size / 5, size / 2, 0, textColor);

        if (currentFill > 0 && !capacityMessage.isFinished()) {
            capacityMessage.render(batch);
        }
    }

    public void update(float delta) {
        tweenManager.update(delta);
        lifetime++;

        float sizeGrow = 10;
        float outerSizeGrow = 13;

        if (!finished) {
            if (finishAnimation) {
                size -= sizeGrow * 3;
                outerSize -= outerSizeGrow * 2;
                finished = (size <= 0);
            } else {
                size += sizeGrow;
                outerSize += outerSizeGrow;

                size = (size > MAX_SIZE) ? MAX_SIZE : size;
                outerSize = (outerSize > FRAME_SIZE) ? FRAME_SIZE : outerSize;
            }
        }

        if (currentFill > 0) {
            capacityMessage.update(delta);
        }
    }

    @Override
    public void finish() {
        finishAnimation = true;
    }

    @Override
    public boolean hasEnded() {
        return (lifetime >= lifespan);
    }

    @Override
    public boolean isFinishing() {
        return finishAnimation;
    }

    public void addOne() {
        lifespan += .4f;
        float target = (currentFill >= capacity) ? capacity : currentFill + 1;
        float duration = (currentFill + 2 >= capacity) ? .1f : .1f;
        tweenManager.update(1000);
        animating = false;
        Tween.to(this, FillUpNoteTween.FILL, duration)
                .target(target)
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        animating = false;
                    }
                })
                .start(tweenManager);

        if (target != currentFill) {
            sizeOffset += 20;
            float messageX = (position.x > Dimen.WIDTH / 2) ? position.x - MAX_SIZE * 2 - sizeOffset : position.x + MAX_SIZE + sizeOffset / 2;
            capacityMessage = new Message(messageX, position.y, getNoteColor(), (int) Math.ceil(currentFill + .1f) + "/" + capacity, 20);
        }
    }

    public int getCurrentFill() {
        return (int) currentFill;
    }

    public int getCapacity() {
        return capacity;
    }

    public boolean containsPoint(Vector2 point) {
        return (
                point.x >= position.x - MAX_SIZE
                        && point.x <= position.x + MAX_SIZE
                        && point.y >= position.y - MAX_SIZE
                        && point.y <= position.y + MAX_SIZE
        );
    }

    public Color getNoteColor() {
        return noteColor;
    }

    public Color getRingColor() {
        return ringColor;
    }

    public void setNoteColor(Color noteColor) {
        this.noteColor = new Color(noteColor.r, noteColor.g, noteColor.b, noteColor.a);
    }

    public Vector2 getPosition() {
        return position;
    }


    public float getSizeOffset() {
        return sizeOffset;
    }

    public void setSizeOffset(float sizeOffset) {
        this.sizeOffset = sizeOffset;
    }

    public void setCurrentFill(float currentFill) {
        this.currentFill = currentFill;
    }
}
