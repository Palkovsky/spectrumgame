package pl.dawidmacek.spectrum.objects;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;

import pl.dawidmacek.spectrum.drawing.Fonts;

public class Message {

    private static final float GROWTH_SPEED = .2f;
    private static final float SHADOW_OFFSET = 6;

    private float x, y;
    private Color shadowColor;
    private String text;
    private int lifespan;
    private int lifetime;
    private float fontScale;
    private boolean finished;
    private boolean positive;


    public Message(float x, float y, Color shadowColor, String text, int lifespan) {
        this.x = x;
        this.y = y;
        this.shadowColor = shadowColor;
        this.text = text;
        this.lifespan = lifespan;
        this.lifetime = 0;
        this.fontScale = .1f;
        this.finished = false;

        Fonts.getInstance().messageFont.getData().setScale(1);
    }

    public void update(float delta) {
        lifetime++;

        if (lifetime >= lifespan) {
            fontScale = (fontScale > .1f) ? (fontScale - GROWTH_SPEED) : .1f;
            if (fontScale <= GROWTH_SPEED)
                finished = true;
        } else {
            fontScale = (fontScale < 1) ? (fontScale + GROWTH_SPEED) : 1;
        }
    }

    public void render(Batch batch) {
        Fonts.getInstance().messageFont.getData().setScale(fontScale);
        Fonts.getInstance().messageFont.setColor(shadowColor);
        Fonts.getInstance().messageFont.draw(batch, text, x - SHADOW_OFFSET, y - SHADOW_OFFSET);
        Fonts.getInstance().messageFont.setColor(Color.BLACK);
        Fonts.getInstance().messageFont.draw(batch, text, x, y);
    }


    public boolean isFinished() {
        return finished;
    }

    public float width() {
        return text.length() * Fonts.getInstance().messageFont.getSpaceWidth() * 2;
    }

    public boolean isPositive() {
        return positive;
    }

    public void setPositive(boolean positive) {
        this.positive = positive;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
