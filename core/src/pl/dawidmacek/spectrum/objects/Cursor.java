package pl.dawidmacek.spectrum.objects;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.equations.Expo;
import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.ui.tweens.Vector2Tween;
import pl.dawidmacek.spectrum.utils.NumberUtils;
import pl.dawidmacek.spectrum.utils.Vertices;

public class Cursor {

    private static final float RADIUS = 40;

    //Rendering
    private EarClippingTriangulator triangulator;
    private TextureRegion cursorTextureRegion;
    private PolygonSprite cursorSprite;

    //Utils
    private TweenManager tweenManager;

    //Data
    private Vector2 position;
    private Color cursorColor;

    public Cursor(Color cursorColor) {
        this.triangulator = new EarClippingTriangulator();
        this.cursorTextureRegion = new TextureRegion(new Texture(Gdx.files.internal("graphics/white.png")));
        this.cursorColor = cursorColor;
        this.cursorColor.a = .7f;
        this.tweenManager = new TweenManager();
        this.position = new Vector2(-100, -100);
    }

    public void update(float delta) {
        tweenManager.update(delta);
    }

    public void render(PolygonSpriteBatch batch) {
        float[] cursorVertices = Vertices.circleSegment(position.x, position.y, RADIUS, 0, 361);
        PolygonRegion cursorPolygonRegion = new PolygonRegion(cursorTextureRegion, cursorVertices, triangulator.computeTriangles(cursorVertices).toArray());
        if (cursorSprite == null)
            cursorSprite = new PolygonSprite(cursorPolygonRegion);
        else
            cursorSprite.setRegion(cursorPolygonRegion);
        cursorSprite.setColor(cursorColor);
        cursorSprite.draw(batch);
    }

    public void goTo(Vector2 target) {
        goTo(target, null);
    }

    public void goTo(Vector2 target, final FinishListener listener) {
        float distance = (float) Math.sqrt(Math.pow(target.y - position.y, 2) + Math.pow(target.x - position.x, 2));
        float duration = (float) NumberUtils.scale(distance, 100, Dimen.WIDTH - 100, .3f, .2f);
        Tween.to(position, Vector2Tween.POS_XY, duration)
                .target(target.x, target.y)
                .ease(Expo.INOUT)
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        if (listener != null)
                            listener.onFinish();
                    }
                })
                .start(tweenManager);

    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Vector2 getPosition() {
        return position;
    }

    public interface FinishListener {
        void onFinish();
    }
}
