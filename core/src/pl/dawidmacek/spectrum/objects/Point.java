package pl.dawidmacek.spectrum.objects;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquation;
import aurelienribon.tweenengine.TweenManager;
import pl.dawidmacek.spectrum.config.B2DFilters;
import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.config.modes.GameMode;
import pl.dawidmacek.spectrum.ui.tweens.Vector2Tween;
import pl.dawidmacek.spectrum.utils.Box2DHelper;
import pl.dawidmacek.spectrum.utils.Units;


public class Point {

    //Config
    public static final float DISPOSE_X = -Dimen.WIDTH * 3.5f;

    //Data
    private Vector2 position;
    private List<Vector2> trail;
    private boolean animating;
    private float currentTarget;
    private GameMode gameMode;
    private TweenEquation easing;
    private Color groundColor;
    private float boost;

    //Rendering
    private PolygonSprite groundSprite;
    private TextureRegion groundTextureRegion;
    private float[] groundVertices;
    private EarClippingTriangulator triangulator;
    private Sprite borderLine;

    //Handlers
    private TweenManager tweenManager;

    //Box2D
    private World world;
    private Body groundBody;

    public Point(World world, float x, float y, GameMode gameMode) {
        this.world = world;
        this.position = new Vector2(x, y);
        this.animating = false;
        this.gameMode = gameMode;
        this.currentTarget = 0;
        this.boost = 0;
        trail = new ArrayList<Vector2>();
        groundColor = Color.ROYAL;

        tweenManager = new TweenManager();

        triangulator = new EarClippingTriangulator();
        groundVertices = new float[]{};

        Texture whiteTexture = new Texture(Gdx.files.internal("graphics/white.png"));

        groundTextureRegion = new TextureRegion(whiteTexture);
        PolygonRegion polygonRegion = new PolygonRegion(groundTextureRegion, groundVertices, triangulator.computeTriangles(groundVertices).toArray());

        groundSprite = new PolygonSprite(polygonRegion);
        //groundColor.a = 1;
        groundSprite.setColor(groundColor);

        borderLine = new Sprite(whiteTexture);
        borderLine.setColor(Color.FIREBRICK);
        borderLine.setSize(30, Dimen.HEIGHT * 10);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        groundBody = world.createBody(bodyDef);
        createFixtures();
    }

    private void createFixtures() {

        float[] vertices = new float[trail.size() * 2];

        for (int i = 0; i < vertices.length; i += 2) {
            vertices[i] = Units.getPPM(trail.get(i / 2).x);
            vertices[i + 1] = Units.getPPM(trail.get(i / 2).y);
        }


        ChainShape shape = new ChainShape();
        shape.createChain(vertices);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.filter.categoryBits = B2DFilters.BIT_GROUND;
        fixtureDef.filter.maskBits = B2DFilters.BIT_HEAD | B2DFilters.BIT_SEGMENT;
        fixtureDef.friction = 1f;
        fixtureDef.restitution = 0f;
        fixtureDef.density = 1f;
        fixtureDef.shape = shape;

        groundBody.createFixture(fixtureDef).setUserData(this);

        shape.dispose();
    }

    public void update(float delta) {
        tweenManager.update(delta);

        for (int i = 0; i < groundBody.getFixtureList().size; i++) {
            groundBody.destroyFixture(groundBody.getFixtureList().get(i));
        }

        Vector2 newVector = new Vector2(position.x, position.y);
        trail.add(newVector);

        Iterator<Vector2> iterator = trail.iterator();
        while (iterator.hasNext()) {
            Vector2 p = iterator.next();

            p.x -= gameMode.xSpeed() + boost;


            if (p.x < DISPOSE_X) {
                iterator.remove();
            }
        }


        groundVertices = new float[trail.size() * 2 + 4];
        int j = 0;
        for (int i = 0; i < groundVertices.length - 4; i += 2) {
            groundVertices[i] = trail.get(j).x;
            groundVertices[i + 1] = trail.get(j).y;
            j++;
        }

        groundVertices[groundVertices.length - 4] = getPosition().x;
        groundVertices[groundVertices.length - 3] = -Dimen.HEIGHT * 5;
        groundVertices[groundVertices.length - 2] = groundVertices[0];
        groundVertices[groundVertices.length - 1] = -Dimen.HEIGHT * 5;

        createFixtures();
    }

    public void render(PolygonSpriteBatch spriteBatch) {
        PolygonRegion groundPolygonRegion = new PolygonRegion(groundTextureRegion, groundVertices, triangulator.computeTriangles(groundVertices).toArray());
        groundSprite.setRegion(groundPolygonRegion);
        groundSprite.draw(spriteBatch);

        borderLine.setPosition(getPosition().x - borderLine.getWidth(), -Dimen.HEIGHT * 2);
        borderLine.draw(spriteBatch);
        borderLine.setPosition(DISPOSE_X - borderLine.getWidth(), -Dimen.HEIGHT * 2);
        borderLine.draw(spriteBatch);
    }

    public void disposeBody() {
        Box2DHelper.disposeBody(groundBody);
        groundBody = null;
    }

    public void dispose() {
    }

    public double calculateAngleForPoint(float x) {
        if (trail.size() > 1) {

            int closestIndex = 0;

            for (int i = 0; i < trail.size(); i++) {
                Vector2 v2 = trail.get(i);

                if (v2.x < x)
                    closestIndex = i;
            }

            Vector2 closestZero = trail.get(closestIndex);
            Vector2 closestWidth;
            if (closestIndex == trail.size() - 1)
                closestWidth = trail.get(closestIndex - 1);
            else
                closestWidth = trail.get(closestIndex + 1);

            double angle = Math.toDegrees(Math.atan2(closestZero.y - closestWidth.y, closestZero.x - closestWidth.x));
            angle = (angle < 0) ? (angle + 360) : angle;

            return angle - 180;
        }
        return 0;
    }

    public void moveVertically(float y, float duration) {
        moveVertically(y, duration, easing);
    }

    public void moveVertically(float y, float duration, TweenEquation e) {
        tweenManager.killAll();
        animating = true;
        this.currentTarget = y;
        Tween.to(position, Vector2Tween.POS_Y, duration)
                .target(y)
                .ease(e)
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int type, BaseTween<?> source) {
                        animating = false;
                    }
                })
                .start(tweenManager);
    }

    public void setEasing(TweenEquation easing) {
        this.easing = easing;
    }

    public TweenEquation getEasing() {
        return easing;
    }

    public Vector2 getPosition() {
        return position;
    }

    public float getCurrentTarget() {
        return currentTarget;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public void setPosition(float x, float y) {
        this.position.x = x;
        this.position.y = y;
    }

    public boolean isAnimating() {
        return animating;
    }

    public float getBoost() {
        return boost;
    }

    public void setBoost(float boost) {
        this.boost = boost;
    }
}
