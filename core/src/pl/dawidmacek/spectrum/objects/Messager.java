package pl.dawidmacek.spectrum.objects;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;

import java.util.ArrayList;
import java.util.List;

import pl.dawidmacek.spectrum.config.Dimen;
import pl.dawidmacek.spectrum.utils.RandomUtil;

public class Messager {

    private static final int MESSAGE_LIFESPAN = 100;

    private Message[] messages;
    private RandomUtil rand;

    public Messager() {
        messages = new Message[4];
        rand = new RandomUtil();
    }

    public void render(Batch batch) {
        for (Message message : messages) {
            if (message != null)
                message.render(batch);
        }
    }

    public void update(float delta) {
        for (int i = 0; i < messages.length; i++) {
            if (messages[i] != null) {
                messages[i].update(delta);
                messages[i] = (messages[i].isFinished()) ? null : messages[i];
            }
        }
    }

    public void send(String text, Color color, boolean important) {
        List<Integer> freeMessagesIndexes = new ArrayList<Integer>();
        for (int i = 0; i < messages.length; i++) {
            if (messages[i] == null)
                freeMessagesIndexes.add(i);
        }
        if (freeMessagesIndexes.size() > 0)
            send(text, color, important, freeMessagesIndexes.get(rand.randInt(0, freeMessagesIndexes.size() - 1)));
    }

    public void send(String text, Color color, boolean important, int num) {
        //Important functionality not applied to case 2 and 3 yet.
        switch (num) {
            //Left Top
            case 0:
                if(important){
                    messages[0] = new Message(100, Dimen.HEIGHT - 60, color, text, MESSAGE_LIFESPAN);
                }else if(messages[0] == null){
                    messages[0] = new Message(100, Dimen.HEIGHT - 60, color, text, MESSAGE_LIFESPAN);
                }
                break;
            //Left Bottom
            case 1:
                if(important) {
                    messages[1] = new Message(100, 150, color, text, MESSAGE_LIFESPAN);
                }else if(messages[1] == null){
                    messages[1] = new Message(100, 150, color, text, MESSAGE_LIFESPAN);
                }
                break;
            //Right Top
            case 2:
                messages[2] = (important) ? new Message(Dimen.WIDTH - 160, Dimen.HEIGHT - 60, color, text, MESSAGE_LIFESPAN) : messages[2];
                messages[2].setX(Dimen.WIDTH - messages[2].width());
                break;
            //Right Bottom
            case 3:
                messages[3] = new Message(Dimen.WIDTH - 160, 150, color, text, MESSAGE_LIFESPAN);
                messages[3].setX(Dimen.WIDTH - messages[3].width());
                break;
        }
    }
}
